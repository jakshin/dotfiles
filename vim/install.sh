#!/usr/bin/env bash
# Installs settings for vim. They're useful mostly on recent macOS versions,
# where vim is preinstalled but doesn't have syntax highlighting on by default.
# They work elsewhere, including on Haiku when installed at ~/config/settings/vimrc,
# but Haiku and Linux distros ship vim with decent default settings anyway.

set -e
script_dir="$(cd -- "$(dirname -- "$0")" && pwd -P)"
source "$script_dir/../scripts/utils.sh"
show_help_if_requested "$@"
cd -- "$script_dir"  # Run from this script's directory
ensure_admin_on_msys2
secho title "Installing settings for the vim editor..."

# Ensure vim is installed, and prompt if it doesn't support syntax highlighting
if ! command -v vim > /dev/null; then
	secho trouble "Aborting: vim isn't installed"
	exit 1
elif vim --version | grep -Fq -- -syntax; then
	confirm "Vim wasn't compiled with syntax highlight enabled. Install these settings anyway?"
	[[ $REPLY == "n" ]] && echo "Aborting" && exit || echo
fi

# Create symlink
install_file "$script_dir/vimrc" "$HOME/.vimrc"
echo "Done"
