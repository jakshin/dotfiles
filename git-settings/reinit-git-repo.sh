#!/usr/bin/env bash
# Reinitializes one or more git repos.
# Each argument should be the root directory of a repo, or its .git subdirectory.

set -e
script_dir="$(cd -- "$(dirname -- "$0")" && pwd -P)"
source "$script_dir/../scripts/utils.sh"
show_help_if_requested "$@"
ensure_admin_on_msys2

if [[ ${#@} == 2 && $1 == "--silent" ]]; then
	# Called from install.sh
	shift
	silent=true
fi

repo_count=$#
if [[ $repo_count == 0 ]]; then
	script_name="$(basename -- "$0")"
	echo "Usage: $script_name git-repo-dir [...]"
	exit 1
elif [[ $silent != true ]]; then
	if [[ $repo_count == 1 ]]; then
		secho title "Reinitializing one git repo..."
	else
		secho title "Reinitializing $repo_count git repos..."
	fi
fi

if [[ -z $DOTFILES_DIR ]]; then
	DOTFILES_DIR="$(cd "$(dirname -- "$script_dir")" && pwd)"
	secho trouble "\$DOTFILES_DIR is empty, assuming $DOTFILES_DIR"
fi

trouble_count=0
function trouble() {
	(( trouble_count+=1 ))
	[[ $repo_count == 1 ]] || echo
	secho trouble "$@"
}

function reinit_git_repo() {
	local repo_path=$1  # Repo root dir or .git subdir; can be absolute or relative

	if [[ ! -e $repo_path ]]; then
		trouble "Warning: skipping non-existent directory $repo_path"
		return 0
	elif [[ ! -d $repo_path ]]; then
		trouble "Warning: skipping non-directory $repo_path"
		return 0
	elif [[ "$(basename -- "$repo_path")" == ".git" ]]; then
		repo_path="$(dirname -- "$repo_path")"
	else
		if [[ ! -d "$repo_path/.git" ]]; then
			trouble "Warning: $repo_path isn't a git repo's root directory"
			return 0
		fi
	fi

	echo
	secho heading "Reinitializing repo: $repo_path"
	cd "$repo_path"

	if [[ -d .git/hooks ]]; then
		cd .git/hooks
		local broken=() suspect=() link_target

		for hook in *; do
			if [[ $hook == "*" ]]; then
				break  # Empty hooks directory
			elif [[ ! -L $hook ]]; then
				continue  # Leave non-symlinks alone
			fi

			if [[ ! -e $hook ]]; then
				broken+=("$hook")
			elif [[ -n $DOTFILES_DIR ]]; then
				link_target="$(readlink "$hook")"
				if [[ $link_target != "$DOTFILES_DIR"* ]]; then
					suspect+=("$hook")
				fi
			fi
		done

		confirm_and_remove_symlinks "broken" "${broken[@]}"
		confirm_and_remove_symlinks "suspect" "${suspect[@]}"
		cd ../..
	fi

	git init
	cd .git/hooks
	colored_ls -dhl -- *
}

function confirm_and_remove_symlinks() {
	local desc=$1
	shift

	if [[ ${#@} == 0 ]]; then
		return 0  # Nothing to do
	fi

	if [[ ${#@} == 1 ]]; then
		echo "Found a $desc symlink in the repo's hooks directory:"
		colored_ls -hl "$@"
		confirm "Remove it?"
	else
		echo "Found some $desc symlinks in the repo's hooks directory:"
		colored_ls -hl "$@"
		confirm "Remove them?"
	fi

	echo
	if [[ $REPLY == "y" ]]; then
		rm -f "$@"
	fi
}

for arg; do
	(reinit_git_repo "$arg")
done

exit "$trouble_count"
