#!/usr/bin/env bash
# Installs my global git settings, using some files in this directory.
# (The git-* files are referred to in my .bashrc, not installed.)

# Git can store its global config in ~/.gitconfig, or $XDG_CONFIG_HOME/git/config;
# here's one way to find it:
# git config --global dummy.foo foo
# cfg_path="$(git config --global --show-origin dummy.foo | awk -F '[:\t]' '{ print $2 }')"
# git config --global --remove dummy

set -e
script_dir="$(cd -- "$(dirname -- "$0")" && pwd -P)"
source "$script_dir/../scripts/utils.sh"
show_help_if_requested "$@"
cd -- "$script_dir"  # Run from this script's directory
ensure_admin_on_msys2
secho title "Configuring git..."

# Standard git settings, applicable anywhere
current_name="$(git config --global user.name || true)"
if [[ -n $current_name ]]; then
	echo "Name is configured: $current_name"
else
	read -rp "Full name: " new_name
	[[ -z $new_name ]] || git config --global user.name "$new_name"
fi

current_email="$(git config --global user.email || true)"
if [[ -n $current_email ]]; then
	echo "Email address is configured: $current_email"
else
	read -rp "Email address: " new_email
	[[ -z $new_email ]] || git config --global user.email "$new_email"
fi

if [[ $OSTYPE == "darwin"* ]]; then
	git config --global credential.helper "osxkeychain"
	echo "Credential helper: $(git config --global credential.helper)"

	git config --global core.excludesfile "$PWD/gitignore-mac"  # Ignore .DS_Store everywhere
	echo "Global gitignore: $(git config --global core.excludesfile)"
fi

git config --global include.path "$PWD/gitconfig-base"
echo "Include path: $PWD/gitconfig-base"

git config --global init.templatedir "$PWD/git-template"
echo "Template directory: $PWD/git-template"

if [[ ! -e git-template/hooks/pre-push || 
	"$(readlink "git-template/hooks/pre-push")" != ""$PWD/confirm-push.sh"" ]];
then
	echo
	secho heading "Configuring the template directory's hooks..."
	mkdir -p git-template/hooks
	rm -f git-template/hooks/pre-push  # In case it's an invalid symlink	

	symlink "$PWD/confirm-push.sh" git-template/hooks/pre-push
	(cd git-template/hooks && colored_ls -Ald -- *)

	if [[ $OSTYPE == "msys" ]]; then
		# On MSYS2, 'git init' turns the template's pre-push symlink into a normal file in .git/hooks
		# Remove it so we'll replace it below, in case confirm-push.sh got updated
		rm -f ../.git/hooks/pre-push
	fi

	./reinit-git-repo.sh --silent "$(dirname "$PWD")"
fi

echo "Done"
