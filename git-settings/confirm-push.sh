#!/usr/bin/env bash
#
# This script is meant to be run as a git pre-push hook.
# It prompts for confirmation if you push to a branch named "main" or "master".
# (But not a _tag_ named main or master).
#
# For best results, install globally as a template via install.sh,
# so git will copy a link to it into each repo you clone or init.
#
# Or you can install it as a pre-push hook in a single repo:
# cd /path/to/repo-root
# ln -s ~/wherever/confirm-push.sh .git/hooks/pre-push
#
# When git calls this script:
# $1 = Name of the remote to which the push is being done
# $2 = URL to which the push is being done
#
# Git supplies info about the commits being pushed as lines on stdin in the form:
# <local-ref> <local-object-id> <remote-ref> <remote-object-id>
#
# Examples:
# refs/heads/main 70a6b532e4113c480ea1fef97e7cef5ba3d6dc3f refs/heads/main ad9b55bc1260092cae40ec39abc3af43a3cefb4b
# HEAD 1098a835ff346d54beca8324e7248092ba512308 refs/heads/main ad9b55bc1260092cae40ec39abc3af43a3cefb4b
# refs/heads/new-branch 1098a835ff346d54beca8324e7248092ba512308 refs/heads/new-branch 0000000000000000000000000000000000000000

set -e  # Abort the push on any error

# shellcheck disable=SC2034
if ! read -rt 3 local_ref local_oid remote_ref remote_oid; then
  # This shouldn't happen in normal operation
  # Try to get the local ref name, and assume we're pushing to the same-named remote ref
  remote_ref="$(git symbolic-ref HEAD)"
fi

if [[ $remote_ref != 'refs/heads/main' && $remote_ref != 'refs/heads/master' ]]; then
  exit 0  # Not pushing to a protected branch, silently continue
fi

branch=${remote_ref/*\//}

if [[ -n $XPC_SERVICE_NAME && $XPC_SERVICE_NAME != 0 ]]; then
  # Apparently launched by some GUI tool (IntelliJ, VS Code, Tower, ...)
  # Let's use a GUI confirmation dialog
  gui=true
else
  gui=false
fi

if [[ $gui == true ]]; then
  if [[ $XPC_SERVICE_NAME == *"fournova.Tower3"* && $USER != "jason.jackson" ]]; then
    exit 0  # Don't prompt for confirmation in Tower
  fi

  title="Are you sure?"

  message="You are about to push to the \\\"$branch\\\" branch."
  message+=$'\n\n'"Is that what you intended?"

  yes="Yep, Continue"
  no="No, Abort"

  if command which osascript &> /dev/null; then
    # This shows a new-style "vertical" dialog on Big Sur; for a classic dialog,
    # use "display dialog" instead: https://tinyurl.com/DisplayDialogsandAlerts
    osascript -e 'tell application (path to frontmost application as text) ¬
        to display alert "'"$title"'" message "'"$message"'" as critical ¬
        buttons {"'"$no"'", "'"$yes"'"} default button "'"$yes"'" cancel button "'"$no"'"' &> /dev/null
    exit 0

  elif [[ $OS == "Windows"* || -n $WSL_DISTRO_NAME ]] && command which powershell.exe &> /dev/null; then
    # https://stackoverflow.com/a/37979970
    message="${message//\\/\`}"  # PowerShell escapes double-quotes with backticks, not backslashes
    powershell.exe "add-type -As System.Windows.Forms;
        if ([windows.forms.messagebox]::show(\"$message\", '$title', 4) -eq 7) { exit(1) }"
    exit 0

  elif command which zenity &> /dev/null; then
    zenity --question --title="$title" --text="$message" --no-wrap \
           --ok-label="$yes" --cancel-label="$no" &> /dev/null
    exit 0

  elif command which xmessage &> /dev/null; then
    linebreak=$'\n'
    message="${message//$linebreak$linebreak/$linebreak}"
    message="${message//\\/}"  # Strip backslashes

    resolution="$(xrandr 2> /dev/null | grep -F '*' | xargs)"
    fn_args=()

    if [[ -n $resolution ]]; then
      width="${resolution//x*/}"
      if (( width >= 3840 )); then
        # The default text is obnoxiously small at high resolutions
        fn_args=(-fn "-bitstream-*-*-*-*--0-250-0-0-p-0-*-*")
      fi
    fi

    xmessage "${fn_args[@]}" -title "$title" -buttons Yes:0,No -default Yes -print "$message" &> /dev/null
    exit 0
  fi

  # We don't know how to make a GUI confirmation dialog here, so just fall through
fi

# Prompt for confirmation in the terminal
if [[ -n $BLANK_LINE_BEFORE_PROMPT ]]; then
  echo  # Put a blank line before the question, but without repeating it on bad input
fi

cd "$(dirname "$(readlink "${BASH_SOURCE[0]}" || echo "${BASH_SOURCE[0]}")")"
source ../scripts/utils.sh

confirm "You're about to push to the ${branch} branch. Is that what you intended?"

if [[ $REPLY == "y" ]]; then
    echo -e "Okie dokie, continuing push to ${branch}\n"
    exit 0
else
    echo "Aborting push to ${branch}"
    exit 1
fi
