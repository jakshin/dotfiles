#!/usr/bin/env bash
# Applies syntax coloring to text files, using either source-highlight or highlight,
# and sends the result to stdout. Intended to be called automatically by Less.
#
# To set up for use in Less, install either the source-highlight or highlight CLI,
# then: export LESSOPEN='|highlight.sh "%s"'
#
# Copyright (c) 2020 Jason Jackson. MIT license.
#

# Bail if Less gives us a dash as the input file name (source-highlight can't handle stdin)
set -e
input="$1"
test "$input" != "-"

# Less up to at least version 661 backslash-escapes spaces and tabs in the path/filename it passes,
# but still passes the path/filename as a single parameter, so the shell commands in this script
# see the backslashes as part of the path/filename, not as escaping the spaces/tabs following them;
# as a result, the commands below can't find the file, and so the script bails out.
# Less then still opens the file, but syntax highlighting doesn't get applied to it.
#
# Less doesn't do this for vertical tabs in the filename, nor does it escape backslashes themselves.
# Newlines in the filename are also mishandled: on macOS & GhostBSD, `less foo$'\n'bar` results in
# Less invoking `less foo` and then `bar`; it probably constructs a string containing newlines,
# and passes it to system(). On Cygwin on Win11, newlines are just replaced with backslashes,
# and on Haiku, they're stripped entirely. This script can't work around any of that behavior.
#
# I've observed this with Less version 581.2 (as shipped) and v661 on macOS Sonoma,
# version 581.2 on Haiku, version 590 on Ubuntu 23.10, and version 643 on GhostBSD and Cygwin.
input="${input//\\ / }"
input="${input//\\$'\t'/$'\t'}"

# Bail if the input file doesn't exist, is empty, isn't readable, or isn't a regular file
test -s "$input"
test -r "$input"
test -f "$input"  # Symlinks to regular files will pass this test, as -f reads through symlinks

# Bail if neither source-highlight nor highlight is installed
command -v source-highlight > /dev/null || command -v highlight > /dev/null

# Resolve symlinks so we can check the target file's name
while [[ -L $input ]]; do
	input="$(readlink "$input")"
done

# Extract some simple compressed formats to a temp file,
# so we can view them in Less without manually decompressing
temp_dir=""

if [[ ($input == *.gz && $input != *.tar.gz && $input != '.gz') ||
	($input == *.bz2 && $input != *.tar.bz2 && $input != '.bz2') ]]
then
	if stat --version 2>&1 | grep -Fq GNU; then
		stat_args=(-c %s)
	else
		stat_args=(-f %Uz)
	fi

	input_size="$(stat "${stat_args[@]}" "$input")"
	test "$input_size" -le 10000000  # Bail if it's bigger than 10 MB

	[[ $input == *.gz ]] && suffix='.gz' || suffix='.bz2'
	uncompressed_name="$(basename "$input" "$suffix")"
	temp_dir="$(mktemp -d)"
	temp_file="$temp_dir/$uncompressed_name"

	[[ $input == *.gz ]] && decompressor=gunzip || decompressor=bunzip2
	if $decompressor -ck "$input" >> "$temp_file"; then
		input="$temp_file"
	else
		rm -rf "$temp_dir"
		exit 1
	fi
fi

# We don't want to exit on errors anymore,
# in case we have a temp file to clean up
set +e

# Bail if the input file doesn't appear to be text;
# Less will prompt that it "may be a binary file", if it wants to
if command -v file > /dev/null; then
	mime_type="$(file -b --mime-type "$input")"

	if [[ $mime_type != text/* && $mime_type != "application/javascript" && $mime_type != "application/json" ]]; then
		[[ -z $temp_dir ]] || rm -rf "$temp_dir"
		exit 1
	fi
fi

# Apply highlighting and send the result to stdout
shopt -s extglob

if command -v source-highlight > /dev/null; then
	# Handle the input language
	input_file="${input/*\/}"

	case $input_file in
		[Mm]akefile.@(common|inc)|GNUmakefile)
			lang_arg="--src-lang=makefile" ;;
		*.jsx|*.ts)
			lang_arg="--src-lang=js" ;;
		?(.)bashrc?(.original)|?(.)bash_login|?(.)bash_logout|?(.)bash_profile?(.original)|?(.)profile)
			lang_arg="--src-lang=bash" ;;
		?(.)zshrc?(.original)|?(.)zshenv|?(.)zlogin|?(.)zlogout|?(.)zprofile|*.zsh-theme|?(.)yazptrc)
			lang_arg="--src-lang=zsh" ;;
		*)
			lang_arg="--infer-lang" ;;
	esac

	# Handle the output language/style
	self="$(perl -MCwd -e 'print Cwd::abs_path shift' "$0" 2> /dev/null || true)"

	if [[ -z $self ]]; then
		self="${BASH_SOURCE[0]}"
		while [[ -L $self ]]; do
			self="$(readlink "$self")"
		done
	fi

	self_dir="$(dirname "$self")"

	if [[ -e "$self_dir/nordy.style" ]]; then
		outlang_arg="--outlang-def=$self_dir/nordy.outlang"
		style_arg="--style-file=$self_dir/nordy.style"
	else
		outlang_arg="--outlang-def=esc256.outlang"
		style_arg="--style-file=esc256.style"
	fi

	# Add `-n` below to have source-highlight number each line
	source-highlight --failsafe "$lang_arg" "$outlang_arg" "$style_arg" -i "$input"
else
	if [[ $COLORTERM == "truecolor" || $COLORTERM == "24bit" ]]; then
		format="truecolor"
	else
		format="xterm256"
	fi

	case $input_file in
		?(.)bashrc?(.original)|?(.)bash_login|?(.)bash_logout|?(.)bash_profile?(.original)|?(.)profile)
			syntax_arg="--syntax=bash" ;;
		?(.)zshrc?(.original)|?(.)zshenv|?(.)zlogin|?(.)zlogout|?(.)zprofile|*.zsh-theme|?(.)yazptrc)
			# highlight doesn't have zsh-specific syntax, and maps *.zsh -> Bash syntax
			syntax_arg="--syntax=bash" ;;
	esac

	# Add `-l` below to have highlight number each line
	highlight --style=zenburn --out-format="$format" "${syntax_arg[@]}" "$input"
fi

# Clean up
[[ -z $temp_dir ]] || rm -rf "$temp_dir"
