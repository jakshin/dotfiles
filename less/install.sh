#!/usr/bin/env bash
# Installs the highlight.sh script, as a symlink in /usr/local/bin,
# to enable syntax highlighting in less, via source-highlight or highlight.

# Note that attempting to use zsh syntax causes source-highlight v3.1.8 to hang indefinitely;
# the bug is fixed in v3.1.9, and we try to patch 3.1.8's busted zsh.lang below.

set -e
script_dir="$(cd -- "$(dirname -- "$0")" && pwd -P)"
source "$script_dir/../scripts/utils.sh"
show_help_if_requested "$@"
cd -- "$script_dir"  # Run from this script's directory
ensure_admin_on_msys2
secho title "Installing a syntax highlighting script for Less..."

# Prompt if an expected program isn't installed
if ! command -v less > /dev/null; then
	confirm "Less doesn't seem to be installed. Install these settings anyway?"
	[[ $REPLY == "n" ]] && echo "Aborting" && exit || echo
fi

if ! command -v source-highlight > /dev/null && ! command -v highlight > /dev/null; then
	confirm "Neither source-highlight nor highlight seems to be installed. Install these settings anyway?"
	[[ $REPLY == "n" ]] && echo "Aborting" && exit || echo
fi

# Target directory
# (This is wrong on Haiku, but source-highlight & highlight aren't available there anyway)
target_dir="/usr/local/bin"

if [[ ! -d $target_dir ]]; then
	echo && mkdirp "$target_dir" && echo
	created_target_dir=true
fi

warn_if_dir_not_in_path "$target_dir" "$created_target_dir"

if [[ ! -w $target_dir ]] && command -v sudo > /dev/null; then
	sudo="sudo"
else
	sudo=""
fi

# Create symlink
install_file "$script_dir/highlight.sh" "/usr/local/bin/highlight.sh" "$sudo"

# Fix source-highlight v3.1.8's zsh language definition
base_paths=(/usr/share /usr/local/share /opt/homebrew/share)

for base_path in "${base_paths[@]}"; do
	if [[ -d "$base_path/source-highlight" ]]; then
		zsh_lang="$base_path/source-highlight/zsh.lang"

		if [[ -f $zsh_lang ]] && grep -q 'ztcp|"' "$zsh_lang"; then
			echo "Fixing source-highlight 3.1.8's zsh.lang"
			$sudo sed -i~ -e 's/ztcp[|]/ztcp/' "$zsh_lang"
			# $sudo rm "$zsh_lang~"
		fi
	fi
done

echo "Done"
