#!/usr/bin/env zsh
## Downloads the starship prompt's binary from GitHub.
## This is automatically called as needed by the theme() function.
##
## Starship also has its own install script: curl -sS https://starship.rs/install.sh | sh
## It can also be installed via Homebrew and Winget.

set -e
if [[ $1 == "-h" || $1 == "--help" ]]; then
	grep "^##" "$0"
	exit
fi

echo "Downloading the starship theme's binary from GitHub..."

if [[ $OSTYPE == "darwin"* ]]; then
	if [[ $CPUTYPE == "arm64" ]]; then
		archive=starship-aarch64-apple-darwin.tar.gz
	else
		archive=starship-x86_64-apple-darwin.tar.gz
	fi
elif [[ $OS == "Windows"* ]]; then
	archive=starship-x86_64-pc-windows-msvc.zip

	if ! command -v unzip > /dev/null; then
		echo "Error: Can't find the unzip program"
		exit 1
	fi
elif [[ $OSTYPE == "linux-gnu" ]]; then
	archive=starship-x86_64-unknown-linux-gnu.tar.gz
else
	echo "Error: Can't download for this platform"
	echo "OSTYPE: $OSTYPE"
	echo "uname -s: $(uname -s)"
	exit 1
fi

latest_release="$(curl -sL https://api.github.com/repos/starship/starship/releases/latest |
					grep -Eo "\"https:[^\"]*${archive}\"" | sed 's/"//g')"

if [[ -z $latest_release ]]; then
	echo "Error: Couldn't determine starship's latest release"
	exit 1
else
	echo $latest_release
fi

cd "$(dirname "$0")"
mkdir -p starship
cd starship

curl -OL -sS --fail "$latest_release"

if [[ $archive == *.zip ]]; then
	unzip "$archive"
	chmod 755 *.exe
else
	tar -xvf "$archive"
fi

rm "$archive"
