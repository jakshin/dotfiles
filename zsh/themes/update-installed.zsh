#!/usr/bin/env zsh
# Updates zsh prompt themes that have already been installed.

set -e
script_dir="$(cd -- "$(dirname -- "$0")" && pwd -P)"
source "$script_dir/../../scripts/utils.sh"
show_help_if_requested "$@"
cd -- "$script_dir"  # Run from this script's directory
secho title "Updating any installed zsh prompt themes..."

# Find installed themes and update them
updated=0
skipped=0

for dir in *(/N); do
	echo
	secho heading "Updating ${dir}..."

	if [[ -d "$dir/.git" ]]; then
		(cd "$dir" && git pull)
		(( updated+=1 ))  # Use +=1 instead of ++ to work around an apparent zsh bug

	elif [[ -x "download-$dir.zsh" ]]; then
		"./download-$dir.zsh"
		(( updated+=1 ))

	else
		secho trouble "Error: Can't update $dir (don't know how)"
		(( skipped+=1 ))
	fi
done

# Update yazpt if it's installed
if [[ -d ~/.yazpt ]]; then
	echo
	uppity yazpt
	(( updated+=1 ))
fi

# Report what happened
if [[ $updated == 0 && $skipped == 0 ]]; then
	echo "No themes installed"
fi

exit $skipped
