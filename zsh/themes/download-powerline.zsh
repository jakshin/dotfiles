#!/usr/bin/env zsh
## Downloads the powerline prompt's binary from GitHub.
## This is automatically called as needed by the theme() function.

set -e
if [[ $1 == "-h" || $1 == "--help" ]]; then
	grep "^##" "$0"
	exit
fi

echo "Downloading the powerline theme's binary from GitHub..."

if [[ $OSTYPE == "darwin"* ]]; then
	# Powerline doesn't provide an ARM binary for macOS,
	# but its "amd64" binary still works on M1
	os=darwin
elif [[ $OS == "Windows"* ]]; then
	os=windows
elif [[ $OSTYPE == "linux-gnu" ]]; then
	os=linux
elif [[ $OSTYPE == *"bsd"* ]]; then
	os=freebsd
else
	echo "Error: Can't download for this platform"
	echo "OSTYPE: $OSTYPE"
	echo "uname -s: $(uname -s)"
	exit 1
fi

latest_release="$(curl -sL https://api.github.com/repos/justjanne/powerline-go/releases/latest |
					grep -Eo "\"https:[^\"]*powerline-go-${os}-amd64[^\"]*\"" | sed 's/"//g')"

if [[ -z $latest_release ]]; then
	echo "Error: Couldn't determine powerline's latest release"
	exit 1
else
	echo $latest_release
fi

cd "$(dirname "$0")"
mkdir -p powerline
cd powerline

curl -OL -sS --fail "$latest_release"
chmod 755 powerline-go-$os-amd64
