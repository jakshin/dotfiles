# Zsh prompt theme loader.
# To use: Source this file, and call its `themes` and `theme` functions as needed.

jj_themes_dir=${${(%):-%x}:A:h}
jj_theme="${jj_theme:-}"  # Name of the currently selected theme
export jj_theme           # So the theme will "stick" when running `sudo -Es`

# Themes we know how to handle, in theme() and .jj_install_theme()
_jj_known_themes=(basic yazpt agkozak agnoster bullet-train powerlevel10k)

if [[ $OSTYPE == "darwin"* || $OS == "Windows"* || $OSTYPE == "linux-gnu" || $OSTYPE == *"bsd"* ]]; then
	_jj_known_themes+=(powerline)
fi

if [[ $OSTYPE == "darwin"* || $OS == "Windows"* || $OSTYPE == "linux-gnu" ]]; then
	_jj_known_themes+=(starship)
fi

# Lists the available prompt themes.
function themes() {
	# echo "Known themes:"
	# which theme | grep -Eio '\s\([a-z0-9|-]+\)' | sed $'s/[()\t]//g'

	local installed=() available=() theme_name
	for theme_name in "$_jj_known_themes[@]"; do
		if [[ $theme_name == "basic" || -d "$jj_themes_dir/$theme_name" ]]; then
			installed+=("$theme_name")
		elif [[ $theme_name == "yazpt" && -d ~/.yazpt ]]; then
			installed+=("$theme_name")
		else
			available+=("$theme_name")
		fi
	done

	echo "Installed:" "$installed[@]"

	if [[ -n $available ]]; then
		echo "Available:" "$available[@]"
	fi
}

# Loads the given theme.
function theme() {
	# Parse arguments; valid options are -i and --install
	local help=false install=false theme_name="" arg

	for arg; do
		if [[ $arg == "-i" || $arg == "--install" ]]; then
			install=true
		elif [[ $arg != -* && -z $theme_name ]]; then
			theme_name=$arg
		else
			help=true
			break
		fi
	done

	if [[ $theme_name == "p10k" ]]; then
		theme_name="powerlevel10k"
	fi

	if [[ $help == true || -z $theme_name ]]; then
		echo "Usage: $0 <theme-name> [-i|--install]"
		echo 'Run `themes` to see a list of known themes'
		return 1
	elif [[ $_jj_known_themes[(I)$theme_name] == 0 ]]; then
		echo "Unknown theme: $theme_name"
		echo 'Run `themes` to see a list of known themes'
		return 1
	fi

	# Install themes on the fly as needed
	if [[ $theme_name == "yazpt" ]]; then
		local theme_dir=~/.yazpt
	else
		local theme_dir="$jj_themes_dir/$theme_name"
	fi

	if [[ $theme_name != "basic" && ! -d "$theme_dir" ]]; then
		[[ $install == true ]] && local REPLY='y' || local REPLY=''

		while [[ -z $REPLY || ! "YyNn" =~ $REPLY ]]; do
			echo -en "The $theme_name theme isn't installed yet. Install it now? [y|n] "
			read -rk 1
			echo
		done

		if [[ ${REPLY:l} == "y" ]]; then
			echo
			(.jj_install_theme "$theme_name" && echo) || return
		else
			return  # Don't install
		fi
	fi

	# Clean up after the current theme, if any
	setopt prompt_cr prompt_sp prompt_percent no_prompt_bang no_prompt_subst

	if [[ ${(t)prompt_theme} == "array" && $prompt_theme[1] != "restore" ]]; then
		if functions promptinit prompt &> /dev/null; then
			echo "Restoring from promptinit-based changes"
			prompt restore
		fi
	fi

	if type .jj_theme_cleanup > /dev/null; then
		echo "Cleaning up the $jj_theme theme"
		.jj_theme_cleanup
		unfunction .jj_theme_cleanup
	fi

	# Activate the new theme
	jj_theme=$theme_name
	echo "Activating the $jj_theme theme"

	case $theme_name in
		(basic)
			PROMPT=$'\n[%{%F{70}%}%~%{%f%}]\n%# '  # 70 = green
			;;

		(yazpt)
			export YAZPT_COMPILE=false
			source ~/.yazpt/yazpt.zsh-theme
			if [[ $OSTYPE == "darwin"* && $TERM_PROGRAM != "vscode" ]]; then
				yazpt_load_preset "jakshin"
			fi

			function .jj_theme_cleanup() {
				yazpt_plugin_unload
				export YAZPT_COMPILE=false
			}
			;;

		(agkozak)
			source "$jj_themes_dir/agkozak/agkozak-zsh-prompt.plugin.zsh"
			AGKOZAK_BLANK_LINES=1
			AGKOZAK_LEFT_PROMPT_ONLY=1
			AGKOZAK_CUSTOM_SYMBOLS=( '⇣⇡' '⇣' '⇡' '+' 'x' '!' '>' '?' '$')
			AGKOZAK_SHOW_STASH=1
			AGKOZAK_USER_HOST_DISPLAY=0

			AGKOZAK_COLORS_USER_HOST=108
			AGKOZAK_COLORS_PATH=116
			AGKOZAK_COLORS_BRANCH_STATUS=228
			AGKOZAK_COLORS_EXIT_STATUS=174
			AGKOZAK_COLORS_CMD_EXEC_TIME=245

			function .jj_theme_cleanup() {
				agkozak-zsh-prompt_plugin_unload
			}
			;;

		(agnoster)
			source "$jj_themes_dir/agnoster/agnoster.zsh-theme"
			setopt prompt_subst
			DEFAULT_USER="$(whoami)"

			function .jj_theme_cleanup() {
				add-zsh-hook -d precmd prompt_agnoster_precmd
			}
			;;

		(bullet-train)
			source "$jj_themes_dir/bullet-train/bullet-train.zsh-theme"
			setopt prompt_subst
			BULLETTRAIN_CONTEXT_DEFAULT_USER="$(whoami)"

			function .jj_theme_cleanup() {
				unfunction precmd preexec
			}
			;;

		(powerlevel10k)
			[[ -n $__p9k_sourced ]] || local echo_char=$'\n'
			[[ -e ~/.p10k.zsh ]] || cp -v "$jj_themes_dir/.p10k.zsh" ~/.p10k.zsh
			source ~/.p10k.zsh

			if [[ $TERM_PROGRAM == "vscode" ]]; then
				# Remove the os_icon from anywhere in the left prompt
				POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=("${(@)POWERLEVEL9K_LEFT_PROMPT_ELEMENTS:#os_icon}")

				typeset -g POWERLEVEL9K_MODE=ascii
				typeset -g POWERLEVEL9K_VCS_BRANCH_ICON=
				typeset -g POWERLEVEL9K_COMMAND_EXECUTION_TIME_VISUAL_IDENTIFIER_EXPANSION=◷
			fi

			source "$jj_themes_dir/powerlevel10k/powerlevel10k.zsh-theme"
			zle_highlight=(default:fg=254 suffix:fg=243)
			echo -n $echo_char

			if (( ${+ZSH_AUTOSUGGEST_USE_ASYNC} )); then
				# The zsh-autosuggestions plugin's async mode doesn't get along with Powerlevel10k
				unset ZSH_AUTOSUGGEST_USE_ASYNC
				typeset -g _jj_ZSH_AUTOSUGGEST_USE_ASYNC=true
			fi

			function .jj_theme_cleanup() {
				powerlevel10k_plugin_unload
				unset zle_highlight

				if [[ -n $_jj_ZSH_AUTOSUGGEST_USE_ASYNC ]]; then
					# If we forced zsh-autosuggestions' async mode off, turn it back on
					typeset -g ZSH_AUTOSUGGEST_USE_ASYNC=
					unset _jj_ZSH_AUTOSUGGEST_USE_ASYNC
				fi
			}
			;;

		(powerline)
			function powerline_precmd() {
				[[ -n $jj_powerline_bin ]] || jj_powerline_bin="$(cd $jj_themes_dir && echo powerline/*)"
				PS1="$($jj_themes_dir/$jj_powerline_bin -error $? -newline -shell zsh)"
			}

			add-zsh-hook precmd powerline_precmd

			function .jj_theme_cleanup() {
				add-zsh-hook -d precmd powerline_precmd
				unset jj_powerline_bin
			}
			;;

		(starship)
			eval "$("$jj_themes_dir/starship/starship" init zsh)"

			function .jj_theme_cleanup() {
				add-zsh-hook -d precmd starship_precmd
				add-zsh-hook -d preexec starship_preexec
				zle -D zle-keymap-select
				unset STARSHIP_SHELL
			}
			;;

		(*)
			echo "Oops! Unknown theme: $theme_name"
			return 1
			;;
	esac
}

# Installs the given theme.
function .jj_install_theme() {
	local theme_name=$1
	echo "Installing the $theme_name theme..."
	cd "$jj_themes_dir" || return 1

	case $theme_name in
		(yazpt)
			git clone "https://github.com/jakshin/yazpt" ~/.yazpt
			;;

		(agkozak)
			git clone --filter=tree:0 "https://github.com/agkozak/agkozak-zsh-prompt" agkozak
			;;

		(agnoster)
			git clone --filter=tree:0 "https://github.com/agnoster/agnoster-zsh-theme" agnoster
			;;

		(bullet-train)
			git clone --filter=tree:0 "https://github.com/caiogondim/bullet-train.zsh" bullet-train
			;;

		(powerlevel10k)
			git clone --filter=tree:0 "https://github.com/romkatv/powerlevel10k"
			;;

		(powerline)
			./download-powerline.zsh
			;;

		(starship)
			./download-starship.zsh
			;;

		(*)
			echo "Oops! Unknown theme: $theme_name"
			return 1
			;;
	esac
}
