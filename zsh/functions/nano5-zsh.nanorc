# Settings for using nano to edit a multi-line interactive shell command
# (Tab size is handled in edit-command-line-better.zsh)
set afterends
set atblanks
set autoindent
set noconvert
set smarthome
set softwrap
set zap

# Bind Ctrl+B/F and Alt+B/F to move the caret word-by-word; the latter binding also makes
# Alt+arrows move the caret by word in Apple Terminal and Tabby (in iTerm they work automatically),
# but nano doesn't allow Alt+arrows to be re-bound, so elsewhere they switch buffers,
# and Ctrl+arrows move the caret by word (but that conflicts with Mission Control)
bind ^B prevword main
bind ^F nextword main
bind M-b prevword main
bind M-f nextword main
bind F7 linter main
bind F8 formatter main

# Syntax highlighting for zsh, based on https://github.com/scopatz/nanorc/blob/master/zsh.nanorc
# For nano version 5+, with fancy colors: pink, purple, mauve, lagoon, mint, lime, peach, orange, latte
syntax "zsh" "\.zsh$" "\.?(zshenv|zprofile|zshrc|zlogin|zlogout|zsh-theme)$"
header "^#!.*/(env +)?zsh( |$)"

# Default color, if no regex below applies
# This is for when a terminal's theme's default color isn't plain white (gray)
color normal ".*"

# Numbers
color peach "\b[0-9]+\b"

# Keywords
color lagoon "\<(always|do|done|esac|then|elif|else|fi|for|case|if|while|function|repeat|time|until|select|coproc|nocorrect|foreach|end|declare|export|float|integer|local|readonly|typeset)\>"

# Builtins
color lagoon "\<(alias|autoload|bg|bindkey|break|builtin|bye|cap|cd|chdir|clone|command|comparguments|compcall|compctl|compdescribe|compfiles|compgroups|compquote|comptags|comptry|compvalues|continue|declare|dirs|disable|disown)\>"
color lagoon "\<(echo|echotc|echoti|emulate|enable|eval|exec|exit|export|false|fc|fg|float|functions|getcap|getln|getopts|hash|history|integer|jobs|kill|let|limit|local|log|logout|noglob|popd|print|printf|pushd|pushln|pwd|r|read|readonly|rehash|return)\>"
color lagoon "\<(sched|set|setcap|setopt|shift|source|stat|suspend|test|times|trap|true|ttyctl|type|typeset|ulimit|umask|unalias|unfunction|unhash|unlimit|unset|unsetopt|vared|wait|whence|where|which)\>"
color lagoon "\<(zcompile|zformat|zftp|zle|zmodload|zparseopts|zprof|zpty|zregexparse|zsocket|zstyle|ztcp)\>"

# Punctuation for conditionals, redirection, etc
color white "(\{|\}|\(\(|\)\)|\]|\[|`|\\|\$|<|>|!|=|&|\|)"
color white "[0-9]>"

# Variables
icolor brightwhite "\$\{?[0-9A-Z_!@#$*?-]+\}?"

# Strings
color lime ""(\\.|[^"])*""
color lime "'(\\.|[^'])*'"

# Comments
color latte "(^|[[:space:]])#.*$"
