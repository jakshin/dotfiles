# Edit the command line using micro or nano, with cursor positioning and zsh syntax highlighting.
# Based on /usr/share/zsh/5.7.1/functions/edit-command-line, with my changes, some of which were inspired by
# https://stackoverflow.com/questions/27773180/zsh-zle-widget-edit-command-line-returns-with-an-error.

function edit-command-line-better() {
	emulate -L zsh

	local editor=${VISUAL:-$EDITOR}

	if [[ -n $editor ]] && ! which "$editor" &> /dev/null; then
		editor=""
	elif [[ $editor == "nano" ]]; then
		if [[ $OSTYPE == "darwin"* && "$(nano -version 2>&1)" == *"Pico"* ]]; then
			editor=""
			local nano_is_pico=true
		fi
	fi
		
	if [[ -z $editor ]]; then
		if which micro > /dev/null; then
			editor="micro"

		elif which nano > /dev/null && [[ $nano_is_pico != true ]] &&
			[[ ($OSTYPE != "darwin"* || "$(nano -version 2>&1)" != *"Pico"*) ]];
		then
			editor="nano"
		
		elif which vim > /dev/null; then
			editor="vim"

		else
			tput bel
			return
		fi
	fi

	# Write the pre-buffer (when at a secondary prompt, the 0-n lines before the one the cursor is currently in)
	# and then the buffer (the line the cursor is in, which could actually be multiple display lines on the screen)
	# to a temp file, then pass that temp file's name to the inner function; it'll be automatically deleted
	() {
		exec </dev/tty

		setopt localoptions nomultibyte noksharrays  # Compute the cursor's position in bytes, not characters
		(( $+zle_bracketed_paste )) && print -r -n - $zle_bracketed_paste[2]

		# Figure out where the cursor is, in line/column terms; this logic isn't perfect,
		# but it works with the fast-syntax-highlighting plugin, and multibyte characters like emoji
		local col_num=1 line_num=$(( $(echo -n $PREBUFFER | wc -l) + 1 ))  # 1-based
		local char_num=0  # 0-based because $CURSOR counts characters in $BUFFER left/above the cursor

		while (( char_num < $CURSOR )); do
			(( char_num++ ))
			local ch=$BUFFER[$char_num]

			if [[ $ch == $'\n' ]]; then
				col_num=1
				(( line_num++ ))
			elif (( col_num >= $COLUMNS )); then
				col_num=2
				(( line_num++ ))
			else
				(( col_num++ ))

				# Nano expands tabs before counting columns, but micro doesn't,
				# i.e. it always counts a tab as one column (as of v2.0.10)
				if [[ $ch == $'\t' && $editor == "nano" ]]; then
					while (( col_num % 8 != 1 )); do
						(( col_num++ ))
					done
				fi
			fi
		done

		if [[ $editor == "micro" ]]; then
			# Load the temp file in micro, with tab size set to match the tab-handling logic above
			micro -backup false -filetype zsh -tabsize 8 +$line_num:$col_num $1

		elif [[ $editor == "nano" ]]; then
			# Load the temp file in nano, with tab size set to match the tab-handling logic above
			# and Ctrl+X set to automatically save before exiting
			local nano_version="$(nano --version)"
			nano_version="${nano_version/* version /}"
			nano_version="${nano_version/.*/}"

			local nano_args=(--syntax=zsh --tabsize=8)
			if (( nano_version < 4 )); then
				nano_args+=(--tempfile)
			elif (( nano_version == 4 )); then
				nano_args+=(--tempfile --rcfile="$funcfiletrace[1]:h/nano4-zsh.nanorc")
			else
				# Starting in 5.0, --tempfile is deprecated, replaced by --saveonexit
				nano_args+=(--saveonexit --rcfile="$funcfiletrace[1]:h/nano5-zsh.nanorc")
			fi

			nano "$nano_args[@]" +$line_num,$col_num $1

		elif [[ $editor == "vim" ]]; then
			vim +$line_num $1
		else
			$editor $1
		fi

		(( $+zle_bracketed_paste )) && print -r -n - $zle_bracketed_paste[1]

		if [[ -z $PREBUFFER ]]; then
			# Alt+E was used before Enter was pressed ($PS2 was never displayed, in other words),
			# so we can take over the current command line directly with the edited version
			BUFFER="$(<$1)"
		else
			# We can't clear out $PREBUFFER (it's too late, those lines are already "accepted"),
			# so we'll abort the current command, and start a new command with the edited version;
			# step 1 is to push the contents of the temp file, i.e. the edited command,
			# onto zsh's buffer stack, which zsh will later automatically pop
			print -Rz - "$(<$1)"
		fi
	} =(<<<"$PREBUFFER$BUFFER")

	if [[ -z $PREBUFFER ]]; then
		zle 'reset-prompt'

		# I prefer the UX of moving the cursor to the end of the buffer's last line,
		# over leaving it at whatever index in the buffer it was at when Alt+E was pressed
		local i
		for i in {1..$BUFFERLINES}; do
			zle 'end-of-line'
		done
	else
		# Calling send-break is the only way to clear $PREBUFFER, i.e. previously accepted lines;
		# doing so sets $? to 1, and also causes this function to immediately stop running,
		# so we can't mask the "error" state -- but if yazpt is in use, *it* can mask the error
		local yazpt_precmd_index=$precmd_functions[(Ie)yazpt_precmd]
		[[ ${yazpt_precmd_index:-0} == 0 ]] || YAZPT_IGNORE_NEXT_EXIT_ERROR=true

		# Abort the current command, which discards of all lines (including $PREBUFFER,
		# although zsh will add those lines to history, sadly, and there's no apparent way to stop it);
		# zsh then pops the buffer stack, putting our edited command in the new buffer
		zle 'send-break'
	fi
}
