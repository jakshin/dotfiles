#!/usr/bin/env zsh
# Installs zsh completions.

set -e
script_dir="$(cd -- "$(dirname -- "$0")" && pwd -P)"
source "$script_dir/../../scripts/utils.sh"
show_help_if_requested "$@"
cd -- "$script_dir"  # Run from this script's directory
ensure_admin_on_msys2
secho title "Installing zsh completions..."

# Ensure we have a place to install completions
if [[ $OSTYPE == "haiku" ]]; then
	# finddir B_USER_NONPACKAGED_DATA_DIRECTORY + /zsh/site-functions
	completions_dir=~/config/non-packaged/data/zsh/site-functions
else
	completions_dir=/usr/local/share/zsh/site-functions

	if [[ $fpath[(I)$completions_dir] == 0 ]]; then
		secho trouble "Error: $completions_dir isn't listed in \$fpath"
		exit 1
	fi
fi

if [[ ! -d $completions_dir ]]; then
	echo
	confirm "Directory $completions_dir doesn't exist. Create it?"

	if [[ $REPLY == "y" ]]; then
		mkdirp "$completions_dir"
	else
		echo "Aborting"
		exit
	fi
fi

# We might need to use sudo to write in our completions directory
if [[ ! -w $completions_dir ]] && command -v sudo > /dev/null; then
	sudo="sudo"
else
	sudo=""
fi

# Installs completions in the passed directory and array of names, by creating symlinks
function install_completions() {
	local src_dir=$1 comps=$2 i

	for (( i=1; 1 == 1; i++ )) do
		local completion=${(P)${comps}[$i]}
		[[ -n $completion ]] || break
		install_completion "$src_dir/$completion" "$completion"
	done
}

# Installs a single completion, with a different name than the source file
function install_completion() {
	local src_path=$1 completion=$2
	local target="$completions_dir/$completion"

	if [[ -L $target && ! -e $target ]]; then
		$sudo rm -f "$target"  # Silently remove the broken symlink
	fi

	install_file "$src_path" "$target" "short $sudo"
}

# We get some nice completions from the zsh-completions repo,
# but don't want to slow down zsh's startup by blindly using all of them;
# note that these completions might not kick in unless the relevant program is installed
echo
if [[ ! -d "zsh-completions" ]]; then
	secho heading "Cloning the zsh-completions repo..."
	git clone --filter=tree:0 https://github.com/zsh-users/zsh-completions
else
	secho heading "Updating the zsh-completions repo..."
	(cd zsh-completions && git pull)
fi

echo
secho heading "Installing completions into $completions_dir..."

completions=(_golang _node _nvm _ts-node _tsc _yarn)
install_completions "$script_dir/zsh-completions/src" completions

# Install completions for my `grr` gradlew/gradle wrapper script and `theme` function
completions=(_grr _theme)
install_completions "$script_dir" completions

# If Docker Desktop is installed on macOS, use its completions
if [[ $OSTYPE == "darwin"* ]]; then
	docker_path="/Applications/Docker.app/Contents/Resources/etc"

	if [[ -d $docker_path ]]; then
		install_completion "$docker_path/docker.zsh-completion" _docker
	else
		echo "Skipping docker completions: Docker Desktop isn't installed"
	fi
fi

# If npm is installed, set up its completions
if /usr/bin/which npm &> /dev/null; then
	npm_target="$completions_dir/_npm"

	if [[ -L $npm_target && ! -e $npm_target ]]; then
		$sudo rm -f "$npm_target"  # Silently remove the broken symlink
	fi

	if [[ -L $npm_target ]]; then
		echo "Skipping $npm_target -> $(readlink "$npm_target")"
	elif [[ -e $npm_target ]]; then
		echo "Overwriting $npm_target"
	else
		echo "Creating $npm_target"
	fi

	$sudo sh -c "echo '#compdef npm' > $npm_target"
	$sudo sh -c "npm completion >> $npm_target"
	$sudo sh -c "echo '_npm_completion \"\$@\"' >> $npm_target"
else
	echo "Skipping npm completion: npm isn't installed"
fi

# Finish up
if [[ -e ~/.zcompdump ]]; then
	echo "Removing existing ~/.zcompdump"
	rm -f ~/.zcompdump
fi

#autoload bashcompinit && bashcompinit	# Allow use of bash completions
autoload -Uz compinit && compinit -u	# Generates completion cache in ~/.zcompdump
