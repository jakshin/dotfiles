#!/usr/bin/env zsh
# Installs zsh plugins.

plugin_repos=(
	"https://github.com/ohmyzsh/ohmyzsh.git"
	"https://github.com/agkozak/zhooks.git"

	# These two conflict with each other
	# "https://github.com/zdharma-continuum/fast-syntax-highlighting.git"
	"https://github.com/zsh-users/zsh-autosuggestions.git"
)

set -e
script_dir="$(cd -- "$(dirname -- "$0")" && pwd -P)"
source "$script_dir/../../scripts/utils.sh"
show_help_if_requested "$@"
cd -- "$script_dir"  # Run from this script's directory
secho title "Installing zsh plugins..."

# Clone/update git repos
for (( i=1; i <= $#plugin_repos; i++ )); do
	repo=$plugin_repos[$i]
	repo_dir="$(basename "$repo" .git)"

	echo
	if [[ -d $repo_dir ]]; then
		secho heading "Pulling from ${repo}..."
		cd $repo_dir
		git pull
		cd ..
	else
		secho heading "Cloning ${repo}..."
		git clone --filter=tree:0 "$repo"
	fi
done
