# Zsh plugin loader.
# To use: Configure $jj_plugins below, and run install.zsh (one-time operation);
# in .zshrc, source this file, and call its jj_load_plugins function.

jj_plugins_dir="${${(%):-%x}:A:h}"

jj_plugins=(
	"ohmyzsh/lib/clipboard.zsh"  # Path relative to this script
	"ohmyzsh/lib/functions.zsh"  # Needed for the Oh My Zsh plugins below to function
	"ohmyzsh/lib/git.zsh"        # For Oh My Zsh prompt themes like bullet-train
	"ohmyzsh/plugins/node/node.plugin.zsh"
	"zhooks/zhooks.plugin.zsh"
)

# if [[ $OSTYPE == "darwin"* ]]; then
# 	# https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/macos
# 	jj_plugins+=("ohmyzsh/plugins/macos/macos.plugin.zsh")
# fi

if [[ -d "$jj_plugins_dir/zsh-autosuggestions" ]]; then
	jj_plugins+=("zsh-autosuggestions/zsh-autosuggestions.zsh")

	if [[ $LC_TERMINAL == "iTerm2" || $TERMKIT_HOST_APP == "Path Finder" ]]; then
		ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=246'

	elif [[ $OSTYPE == "haiku" ]]; then
		ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=243'
	fi
elif [[ -d "$jj_plugins_dir/fast-syntax-highlighting" ]]; then
	# Load this plugin after other plugins
	# Use the fast-theme() function to list/test/switch syntax themes
	jj_plugins+=("fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh")
fi

function jj_load_plugins() {
	export ZSH="$jj_plugins_dir/ohmyzsh"  # Oh My Zsh plugins expect $ZSH to be set

	# Load plugins, silently skipping any that don't exist
	local i
	for (( i=1; i <= $#jj_plugins; i++ )); do
		local plugin=$jj_plugins[$i]
		[[ -e "$jj_plugins_dir/$plugin" ]] && source "$jj_plugins_dir/$plugin"
	done

	# Get rid of stuff I don't want from ohmyzsh/lib/functions.zsh
	unfunction uninstall_oh_my_zsh upgrade_oh_my_zsh &> /dev/null

	# Clean up
	unset ZSH
	unfunction jj_load_plugins  # This function only needs to be called once
}
