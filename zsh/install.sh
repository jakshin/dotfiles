#!/usr/bin/env bash
# Installs things from this directory's subdirectories.

set -e
script_dir="$(cd -- "$(dirname -- "$0")" && pwd -P)"
source "$script_dir/../scripts/utils.sh"
show_help_if_requested "$@"
cd -- "$script_dir"  # Run from this script's directory

# Run install scripts in subdirectories
./completions/install.zsh && echo
./plugins/install.zsh
