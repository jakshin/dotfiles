-- https://wezfurlong.org/wezterm/config/files.html
-- https://wezfurlong.org/wezterm/config/lua/config/index.html
local wezterm = require 'wezterm'
local config = wezterm.config_builder()

if string.find(wezterm.target_triple, 'apple-darwin', 1, true) then
	-- macOS (see https://wezfurlong.org/wezterm/config/lua/wezterm/target_triple.html)
	config.initial_cols = 145
	config.initial_rows = 48
	config.font_size = 13

elseif string.match(wezterm.target_triple, 'windows') then
	-- Windows (system-wide, not specific to WSL/Cygwin/MSYS2)
	config.initial_cols = 110
	config.initial_rows = 35

	config.launch_menu = {}
	for _, shell in ipairs({
		{ name = 'zsh', path = 'C:\\cygwin64\\bin\\zsh.exe' },
		{ name = 'bash', path = 'C:\\cygwin64\\bin\\bash.exe' },
	}) do
		if #(wezterm.glob(shell.path)) == 1 then
			path = '/usr/local/bin;/usr/bin;' .. os.getenv("PATH")

			table.insert(config.launch_menu, {
				label = 'Cygwin ' .. shell.name,
				args = { shell.path, "-l" },
				set_environment_variables = { PATH = path }
			})

			if not config.default_prog then
				config.default_prog = { shell.path, "-l" }
				config.set_environment_variables = { PATH = path }
			end
		end
	end

	local powershell_args = { 'powershell.exe', '-NoLogo' }
	table.insert(config.launch_menu, { label = 'PowerShell', args = powershell_args })
	if not config.default_prog then
		config.default_prog = powershell_args
	end
else
	-- Assume Linux/BSD
	config.initial_cols = 130
	config.initial_rows = 40
	config.font_size = 11
end

return config
