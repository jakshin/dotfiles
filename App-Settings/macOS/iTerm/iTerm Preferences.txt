Profiles > Colors
	Import J-Squared.itermcolors

Profiles > Text
	Default font is Monaco, MesloLGS NF is better

Profiles > Window
	Larger default size, e.g. 165 x 55

Profiles > Keys
	Left Option key: Esc+
