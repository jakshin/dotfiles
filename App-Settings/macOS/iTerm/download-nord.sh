#!/bin/sh
# Downloads the Nord theme for iTerm
curl -O "https://raw.githubusercontent.com/arcticicestudio/nord-iterm2/develop/src/xml/Nord.itermcolors"
