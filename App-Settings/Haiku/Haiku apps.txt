Useful Haiku apps (available in HaikuDepot)

BePodder			Podcast & RSS client
ColorLines		Easy way to test whether sound is working (it's a game)
Colors			Color picker
cronie			A cron daemon
DejaVu			Font package
FilWip			Cleans up files to save disk space
Fontboy			Font viewer
Koder			Code editor
LuckyBackup		Remote backups; needs cronie to schedule
Moe				Anime window sitter
nanosvgtranslator	Lets ShowImage open SVG files
Open Sound		Sound drivers
Otter Browser		Web browser with rearrangeable tabs
QuickLaunch		Keyboard app launcher (assign Ctrl+Esc in Shortcuts pref app)
Slayer			Functionally like Windows's Task Manager
TaskManager		Visually like Windows's Task Manager
Tipster			Haiku usage tips
VMware add-ons	Like VMware Tools
zsh				Nifty shell

pkgman install bepodder color_lines cronie dejavu filwip fontboy koder luckybackup moe nanosvgtranslator opensound otter_browser quicklaunch slayer taskmanager tipster vmware_addons zsh

Installing Open Sound should make sound work; a reboot is probably needed. Running the ColorLines game is one easy way to test whether sound is working.

To make Moe work, navigate to /boot/system/apps/Moe and configure moe.png to open in Moe, then make a symlink to moe.png somewhere; double-click that symlink to launch Moe.

Interesting & reference apps

LnLauncher		Minimalist app launcher
Paladin			IDE
Qt Creator		IDE
SuperPrefs		Categorized prefs launcher
