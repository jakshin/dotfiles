#!/bin/bash
# Installs my personal bash settings on Haiku;
# the bash settings exist solely to facilitate using zsh by default.

set -e
script_dir="$(cd -- "$(dirname -- "$0")" && pwd -P)"
source "$script_dir/../../../scripts/utils.sh"
show_help_if_requested "$@"
cd -- "$script_dir"  # Run from this script's directory

# Where are we gonna install to?
settings_dir="$(finddir B_USER_SETTINGS_DIRECTORY)"

# Create symlinks
install_file "$script_dir/bash_profile" "$settings_dir/bash_profile"
install_file "$script_dir/bashrc" "$settings_dir/bashrc"
