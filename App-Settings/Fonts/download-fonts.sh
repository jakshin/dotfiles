#!/usr/bin/env bash
# Downloads various fonts to this directory.

set -e
cd "$(dirname -- "$0")"

font_urls=(
	https://github.com/tonsky/FiraCode/releases/download/6.2/Fira_Code_v6.2.zip

	https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Regular.ttf
	https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold.ttf
	https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Italic.ttf
	https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold%20Italic.ttf

	https://github.com/adobe-fonts/source-code-pro/releases/download/2.042R-u%2F1.062R-i%2F1.026R-vf/TTF-source-code-pro-2.042R-u_1.062R-i.zip
)

for font_url in "${font_urls[@]}"; do
	local_file="$(basename "$font_url")"
	local_file=${local_file//%20/ }
	echo "Downloading $font_url => $local_file"

	if type curl &> /dev/null; then
		curl --progress-bar --location --output "$local_file" "$font_url"

	elif type wget &> /dev/null; then
		wget --no-verbose --show-progress -O "$local_file" "$font_url"

	else
		echo "Error: Can't find curl or wget"
		exit 1
	fi
done

if type unzip > /dev/null; then
	unzip -p Fira_Code_v6.2.zip README.txt > "Fira Code README.txt"
	unzip -jo Fira_Code_v6.2.zip "ttf/*"
	unzip -o TTF-source-code-pro-2.042R-u_1.062R-i.zip "*.ttf"
else
	echo "Error: Can't find unzip"
	exit 1
fi
