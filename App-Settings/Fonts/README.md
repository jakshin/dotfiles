MesloLGS is from Nerd Fonts, looks just like Menlo,
and has extra "icon" characters that are useful in shell prompt themes.

Source Code Pro is referenced in the Nord theme for Terminal.

On Linux, if there's no GUI way to install fonts, copy them to ~/.local/share/fonts.   
On Haiku, copy them to $XDG_DATA_HOME/fonts/ttfonts.
