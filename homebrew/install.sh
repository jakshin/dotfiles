#!/usr/bin/env bash
# Offers to install Homebrew if needed, then a judicious selection of Homebrew packages.

declare -a packages=('bash' 'bash-completion' 'editorconfig' 'git' 'go' 'gradle' 'gradle-completion'
                     'jq' 'micro' 'nano' 'nanorc' 'node' 'shellcheck' 'source-highlight' 'trash' 'yarn')
declare -a casks=('qlmarkdown' 'qlstephen')

set -e
script_dir="$(cd -- "$(dirname -- "$0")" && pwd -P)"
source "$script_dir/../scripts/utils.sh"
show_help_if_requested "$@"
cd -- "$script_dir"  # Run from this script's directory
secho title "Installing Homebrew packages..."

# Utilities
blank_line_needed=false

function blank_line_if_needed() {
	if [[ $blank_line_needed == true ]]; then
		blank_line_needed=false
		echo
	fi
}

function ask() {
	blank_line_if_needed  # Blank line before the question, without repeating it on bad input
	confirm "$1"
}

function tell() {
	blank_line_if_needed

	if [[ $1 == bright || $1 == heading || $1 == trouble ]]; then
		local style=$1
		shift
		secho "$style" "$1"
	else
		echo -e "$1"
	fi
}

# Do the things
if ! command -v brew &> /dev/null; then
	blank_line_needed=true
	ask "Homebrew isn't installed yet. Install it now to continue?"

	if [[ $REPLY == "y" ]]; then
		echo
		tell heading "Installing Homebrew..."

		# From https://brew.sh
		# For uninstallation, see https://docs.brew.sh/FAQ#how-do-i-uninstall-homebrew
		/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
		blank_line_needed=true
	else
		exit
	fi
fi

if command -v nano > /dev/null && [[ "$(nano -version 2>&1)" == *"Pico"* ]]; then
	tell trouble "Note: Nano is currently actually just a symlink to pico"
	blank_line_needed=true
fi

cask_arg=()

for package in "${packages[@]}" - "${casks[@]}"; do
	if [[ $package == "-" ]]; then
		cask_arg=(--cask)
		continue
	fi

	if brew ls --versions "${cask_arg[@]}" "$package" &> /dev/null; then
		tell "Package $package is already installed"
	else
		ask "Install $package?"

		if [[ $REPLY == "y" ]]; then
			echo
			secho heading "Installing $package..."
			brew install "$package"
			blank_line_needed=true
		fi
	fi
done

tell "Done"
