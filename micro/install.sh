#!/usr/bin/env bash
# Installs settings for the micro text editor.

set -e
script_dir="$(cd -- "$(dirname -- "$0")" && pwd -P)"
source "$script_dir/../scripts/utils.sh"
show_help_if_requested "$@"
cd -- "$script_dir"  # Run from this script's directory
ensure_admin_on_msys2
secho title "Installing settings for the micro editor..."

# Ensure micro is installed (which it shouldn't be, on Cygwin/MSYS2)
if ! command -v micro > /dev/null; then
	secho trouble "Aborting: micro isn't installed"
	exit 1
fi

# Figure out where we'll install
if [[ -n $MICRO_CONFIG_HOME ]]; then
	cfg_dir="$MICRO_CONFIG_HOME"
else
	cfg_dir="${XDG_CONFIG_DIR:-$HOME/.config}/micro"
fi

mkdir -p "$cfg_dir"

# Create symlinks
install_file "$script_dir/bindings.json" "$cfg_dir/bindings.json"
install_file "$script_dir/settings.json" "$cfg_dir/settings.json"
install_file "$script_dir/colorschemes" "$cfg_dir/colorschemes"
install_file "$script_dir/syntax" "$cfg_dir/syntax"
echo

# Ask micro to install some plugins (requires a network connection)
micro -plugin install detectindent

if [[ $OSTYPE == "darwin"* ]]; then
	micro -plugin install editorconfig
fi
