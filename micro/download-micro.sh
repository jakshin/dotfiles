#!/usr/bin/env bash
# Downloads micro to ~/bin if it's in $PATH, or /usr/local/bin otherwise.
# Useful when an OS's package manager offers an old version (e.g. Ubuntu LTS),
# or doesn't offer micro at all (e.g. Amazon Linux 2, CentOS, OpenSUSE).

set -e
script_dir="$(cd -- "$(dirname -- "$0")" && pwd -P)"
source "$script_dir/../scripts/utils.sh"
show_help_if_requested "$@"
cd -- "$script_dir"  # Run from this script's directory

# Cygwin & MSYS2 aren't officially supported
# https://github.com/zyedidia/micro#cygwin-mingw-plan9
if [[ $OSTYPE == "cygwin" ]]; then
	secho trouble "The Windows version of micro doesn't work well on Cygwin."
	echo "It can't open unix-style paths, including through symlinks, so including my settings files."
	confirm "Continue anyway?"
	[[ $REPLY == "y" ]] || exit

elif [[ $OSTYPE == "msys" ]]; then
	secho trouble "The Windows version of micro doesn't work well on MSYS2."
	echo "It hangs just after launch in mintty, and doesn't draw anything on screen (although it does work in other terminals)."
	confirm "Continue anyway?"
	[[ $REPLY == "y" ]] || exit
fi

# Decide where we'll put micro's single binary
unset sudo sudo_opts

if [[ $PATH == *"$HOME/bin"* ]]; then
	target_dir="$HOME/bin"
else
	target_dir="/usr/local/bin"
fi

secho title "Downloading micro to $target_dir"

mkdirp "$target_dir"
colored_ls -dhl "$target_dir"
echo
warn_if_dir_not_in_path "$target_dir" true

# We might need to use sudo
if [[ ! -w "$target_dir" ]] && command -v sudo > /dev/null; then
	sudo="sudo"
	sudo_opts="-E"
fi

# Download
cd "$target_dir"
echo "Current working directory is now $target_dir"

if [[ -L micro || -e micro ]]; then
	colored_ls -dhl micro
	confirm "$target_dir/micro already exists. Replace it?"

	if [[ $REPLY == "y" ]]; then
		$sudo rm -f micro
		echo
	else
		exit
	fi
fi

if [[ "$(uname -s)" == "MidnightBSD" ]]; then
	export GETMICRO_PLATFORM="freebsd64"
fi

if ! command -v curl &> /dev/null && command -v wget &> /dev/null; then
	wget --no-verbose -O- https://getmic.ro | $sudo $sudo_opts bash
else
	curl -sS --fail https://getmic.ro | $sudo $sudo_opts bash
fi

if [[ -e micro ]]; then
	if [[ -n $sudo ]]; then
		# Inside the tarball, files have UID/GID 1000 (zyedidia:zyedidia),
		# and extracting the tarball as root in the getmic.ro script preserves them
		$sudo chown root:0 micro
	fi

    colored_ls -dhl "$target_dir/micro"*
fi
