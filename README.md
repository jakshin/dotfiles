Dotfiles (and more)

**Installation:**

- Clone the repo to ~/.dotfiles (or wherever)

- Use the `App-Settings` directory to configure apps and some system-wide preferences.   
  Install fonts first (on macOS, they're referenced in Terminal & iTerm settings).   
  On Linux, if there's no GUI way to install fonts, copy them to ~/.local/share/fonts;   
  on Haiku, copy them to $XDG_DATA_HOME/fonts/ttfonts.   

- Run `./install.sh`

- Use `git init` to reinitialize any other existing git repos, so they're configured   
  with the pre-push hook that confirms pushes to the main branch

- See https://github.com/jakshin?tab=repositories for other handy things
