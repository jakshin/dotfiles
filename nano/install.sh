#!/usr/bin/env bash
# Installs settings for GNU nano 2.0.6 on macOS.

set -e
script_dir="$(cd -- "$(dirname -- "$0")" && pwd -P)"
source "$script_dir/../scripts/utils.sh"
show_help_if_requested "$@"
cd -- "$script_dir"  # Run from this script's directory
ensure_admin_on_msys2
secho title "Installing settings for the nano editor..."

# Ensure nano is installed, and prompt if it's actually pico
if ! command -v nano > /dev/null; then
	secho trouble "Aborting: nano isn't installed"
	exit 1
elif [[ "$(nano -version 2>&1)" == *"Pico"* ]]; then
	confirm "Nano is actually pico. Install these settings anyway?"
	[[ $REPLY == "n" ]] && echo "Aborting" && exit || echo

	nano_banner="pico symlinked as nano"
	nano_version=0
else
	nano_banner="$(nano --version | head -n1 | xargs)"
	nano_version="${nano_banner/* version /}"
	nano_version="${nano_version/.*/}"
fi

# Globals & utility functions
if [[ $OSTYPE == "haiku" ]]; then
	# finddir B_USER_NONPACKAGED_DATA_DIRECTORY + /zsh/site-functions
	completions_dir=~/config/non-packaged/data/zsh/site-functions
else
	completions_dir=/usr/local/share/zsh/site-functions
fi

function make_completions_dir_and_install() {
	local link_target_path=$1
	local sudo=""

	if [[ -d $completions_dir ]]; then
		[[ -w $completions_dir ]] || sudo="sudo"
		install_file "$completion_file" "$completions_dir/_nano" "$sudo"
	else
		echo $'\n'"Installing zsh completion for $nano_banner..."
		confirm "Directory $completions_dir doesn't exist. Create it?"

		if [[ $REPLY == "y" ]]; then
			mkdirp "$completions_dir"
			symlink "$link_target_path" "$completions_dir/_nano"
		else
			echo "Okay, never mind then"
		fi
	fi
}

# Which nano settings should we install?
if [[ $OSTYPE == "darwin"* && $nano_version == 2 ]]; then
	echo "Installing for macOS's nano v2.0.6, using nicer-nano-206..."

	echo
	if [[ -d "nicer-nano" ]]; then
		(cd "nicer-nano" && git pull)
		echo
	else
		git clone "https://github.com/jakshin/nicer-nano-206" "nicer-nano"
		echo
	fi

	# Create symlinks
	install_file "$script_dir/nicer-nano/cfg/nanorc" ~/.nanorc
	install_file "$script_dir/nicer-nano/syntax" ~/.nano-syntax
	install_file "$script_dir/nicer-nano/scripts/nano206++.sh" "/usr/local/bin/nano"

	completion_file="$script_dir/nicer-nano/completions/nano.zsh"
	make_completions_dir_and_install "$completion_file"

elif (( nano_version <= 0 || nano_version >= 4 )); then
	echo "Installing for $nano_banner"

	# Create or overwrite ~/.nanorc
	if [[ $nano_version != 0 ]]; then
		new_nanorc="$(
			echo "set afterends"
			echo "set atblanks"
			echo "set autoindent"
			echo "set noconvert"
			echo "set smarthome"
			echo "set softwrap"
			echo "set tabsize 4"
			echo "set zap"
			echo

			command -v aspell > /dev/null && ! command -v hunspell > /dev/null || echo -n "# "
			echo 'set speller "aspell"'

			(( nano_version > 4 )) || echo -n "# "
			echo "set indicator  # Option added in nano v5.0"
			echo

			if [[ $OSTYPE == "haiku" ]]; then
				# Newlines in pasted text are understood by nano as ^J, which invokes "justify" by default
				# Explanation: https://github.com/vercel/hyper/issues/1448#issuecomment-367890105
				echo "bind ^J enter main  # Fix multi-line paste in Haiku Terminal"
			fi

			# On Windows, Linux, BSD, and Haiku, the default keybindings in nano for word-left/word-right
			# use Ctrl+arrows, which conflicts with Mission Control; also bind Ctrl+B and Ctrl+F
			# (Alt+arrows switches files, just like Alt+< and Alt+>, except in Tabby on Windows,
			# where they somehow automatically do word-left/word-right)
			echo "bind ^B prevword main"
			echo "bind ^F nextword main"

			# In Apple Terminal and Tabby on macOS, alt+left is alt+b and alt+right is alt+f;
			# make those keys move the caret word-by-word, and assign different keys for linter & formatter
			if [[ $OSTYPE == "darwin"* ]]; then
				echo "bind M-b prevword main  # Alt+B (or Alt+Left on macOS)"
				echo "bind M-f nextword main  # Alt+F (or Alt+Right on macOS)"
			fi

			echo "bind F7 linter main"
			echo "bind F8 formatter main"
			echo

			# First include syntax definitions shipped with nano itself,
			# then any from https://github.com/scopatz/nanorc, then my own

			if [[ $OSTYPE == "haiku" ]]; then
				# B_SYSTEM_DATA_DIRECTORY is /boot/system/data
				nanorc_paths=("$(finddir B_SYSTEM_DATA_DIRECTORY)/nano")
			else
				nanorc_paths=(
					/usr/share/nano             # Shipped with nano on Linux, Cygwin, MSYS2
					/usr/local/share/nano       # Homebrew's nano package on x86_64, or shipped on BSD
					/usr/local/share/nanorc     # Homebrew's nanorc package on x86_64
					/opt/homebrew/share/nano    # Homebrew's nano package on arm64
					/opt/homebrew/share/nanorc  # Homebrew's nanorc package on arm64
				)
			fi

			nanorc_paths+=(
				~/.nano         # From https://github.com/scopatz/nanorc
				~/.nano-syntax  # Symlink to my own syntax definitions
			)

			for nanorc_path in "${nanorc_paths[@]}"; do
				if [[ -d $nanorc_path ||
					($nanorc_path == ~/.nano-syntax && ! -e $nanorc_path && ! -L $nanorc_path) ]]
				then
					if [[ $BASH_VERSION == 3* ]]; then
						echo "include \"${nanorc_path/$HOME/~}/*.nanorc\""
					else
						echo "include \"${nanorc_path/$HOME/\~}/*.nanorc\""
					fi
				fi
			done
		)"

		if [[ -e ~/.nanorc || -L ~/.nanorc ]]; then
			if [[ -f ~/.nanorc && $new_nanorc == "$(< ~/.nanorc)" ]]; then
				echo "Already exists: ~/.nanorc"
				write_new_nanorc=false
			else
				echo
				colored_ls -dhl ~/.nanorc

				# shellcheck disable=SC2088
				confirm "~/.nanorc exists. Replace it?"

				if [[ $REPLY == "y" ]]; then
					rm -f ~/.nanorc
					echo -e "Overwriting ~/.nanorc with a new one\n"
					write_new_nanorc=true
				else
					echo -e "Leaving the existing ~/.nanorc alone\n"
					write_new_nanorc=false
				fi
			fi
		else
			echo "Creating a new ~/.nanorc"
			write_new_nanorc=true
		fi

		if [[ $write_new_nanorc == true ]]; then
			echo "$new_nanorc" >> ~/.nanorc
		fi

		# Offer syntax definitions (symlink to a directory)
		install_file "$script_dir/syntax" ~/.nano-syntax

		if [[ -L ~/.nano-syntax && "$(readlink ~/.nano-syntax)" == *"$script_dir"* ]]; then
			# shellcheck disable=SC2088
			if ! grep -Fq "~/.nano-syntax" ~/.nanorc &> /dev/null; then
				include='include "~/.nano-syntax/*.nanorc"'
				echo "Appending to ~/.nanorc: $include"
				echo "$include" >> ~/.nanorc
			fi
		fi
	fi

	# Offer zsh completion
	completion_file="$script_dir/completions/nano-completion.zsh"
	make_completions_dir_and_install "$completion_file"

else
	secho trouble "Error: Can't install for nano version $nano_version"
	exit 1
fi

rm -f ~/.zcompdump  # In case we installed completions
