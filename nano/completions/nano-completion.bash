# Bash completion for nano version 4+ (also works for macOS's nano v2.0.6 and pico v5.09).
# Distros and other environments ship various nano versions, compiled with various flags,
# so this parses options from nano's help, taking inspiration from zsh's _gnu_generic.
# To use: run 'source nano-completion.bash'

function _nano() {
	local current_word="${COMP_WORDS[COMP_CWORD]}"
	if [[ $current_word != -* ]]; then
		return  # Nothing to do -- we only complete options
	fi

	# Bail if "--" was already passed, since options have ended
	local i
	for (( i=COMP_CWORD-1; i >= 0; i-- )); do
		if [[ ${COMP_WORDS[$i]} == "--" ]]; then
			return
		fi
	done

	# Read the output from "nano -h", invoked like that in case it's really pico
	if [[ -z $_nano_option_cache ]]; then
		_nano_option_cache=()
		local i lines line words word

		if (( BASH_VERSINFO[0] >= 4 )); then
			mapfile -t lines < <(nano -h)
		else
			IFS=$'\n' read -rd $'\0' -a lines < <(nano -h; printf '\0')
		fi

		for line in "${lines[@]}"; do
			read -ra words <<< "$line"

			if [[ ${words[0]} != -* ]]; then
				continue  # Every line we'll parse starts by listing a short option
			elif [[ ${words[0]} == *, ]]; then
				continue  # One of nano 2.0.6's "-a, -b, -e," lines
			fi

			for i in 0 1 2; do
				word=${words[$i]}
				[[ $word == -* && $word != - ]] || continue

				word="${word/\[*}"  # e.g. pico's -r[#cols]
				if [[ $word == *=* ]]; then
					word="${word/=*}="
				fi

				_nano_option_cache+=("$word")
			done
		done
	fi

	# shellcheck disable=SC2207
	COMPREPLY=( $(compgen -W "${_nano_option_cache[*]}" -- "$current_word") )
}

complete -F _nano -o bashdefault -o default nano
