#compdef nano
# Zsh completion for nano version 4+ (also works for macOS's nano v2.0.6 and pico v5.09).
# Distros and other environments ship various nano versions, compiled with various flags,
# so this parses options from nano's help, taking inspiration from zsh's _gnu_generic.
# To install: symlink this file as "_nano", anywhere in $fpath, then rm -f ~/.zcompdump

if [[ -z $_nano_option_cache ]] && which nano > /dev/null; then
	# Read the output from "nano -h", invoked like that in case it's really pico
	local lines=(${(f)"$(nano -h)"})

	# Split each line into words, which we call "parts" here because $words
	# is a magic variable that zsh automatically sets while a completion is running
	local line parts i s_arg="-s"

	for line in "$lines[@]"; do
		read -rA parts <<< "$line"

		if [[ $parts[1] != -* ]]; then
			continue  # Every line we'll parse starts by listing a short option
		elif [[ $parts[1] == *, ]]; then
			continue  # Nano 2.0.6's "-a, -b, -e," or "-f, -g, -j" line
		fi

		for i in 1 2 3; do
			local part=$parts[$i]
			[[ $part == -* && $part != - ]] || continue

			part="${part/\[*}"  # e.g. pico's -r[#cols]
			if [[ $part == *=* ]]; then
				part="${part/=*}="
			fi

			parts[i]=$part
		done

		local short=$parts[1] long="" explanation=""
		if [[ $parts[3] == --* ]]; then
			long=$parts[3]
			explanation="${parts[@]:3}"  # Array items are 0-indexed here
			short+="+"
		elif [[ $parts[2] == --* ]]; then
			long=$parts[2]
			explanation="${parts[@]:2}"
		else
			# This nano must actually be pico
			s_arg=""
			if [[ $parts[2] == '<'* ]]; then
				explanation="${parts[@]:2}"
			else
				explanation="${parts[@]:1}"
			fi
		fi

		if [[ $short == "-h" || $short == "-V" ]]; then
			# A bit of special handling for the -h/--help and -V/--version options
			short="(- *)$short"
			if [[ -n $long ]]; then
				long="(- *)$long"
			fi
		fi

		# Square brackets enclose explanations, so to keep zsh from complaining,
		# we need to escape any close-square brackets _in_ the explanation
		explanation="${explanation//]/\\]}"

		_nano_option_cache+=("${short}[${explanation}]")
		if [[ -n $long ]]; then
			_nano_option_cache+=("${long}[${explanation}]")
		fi
	done
fi

_arguments $s_arg -S : "$_nano_option_cache[@]" "*:file:_files"
