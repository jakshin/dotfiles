#!/usr/bin/env bash
# Installs symlinks in /usr/local/bin for all scripts in the "std" subdirectory,
# and also the "mac" subdirectory if we're running on macOS.

set -e
script_dir="$(cd -- "$(dirname -- "$0")" && pwd -P)"
source "$script_dir/../scripts/utils.sh"
show_help_if_requested "$@"
cd -- "$script_dir"  # Run from this script's directory
ensure_admin_on_msys2
secho title "Installing some handy shell scripts..."

# Where should we install?
if [[ $OSTYPE == "haiku" ]]; then
	target_dir="$(finddir B_USER_NONPACKAGED_BIN_DIRECTORY)"

elif [[ $OSTYPE == "solaris2"* ]]; then
	# OpenIndiana (may need to create this directory, and add it to PATH in /etc/profile)
	target_dir="/opt/local/bin"
	if [[ $PATH != *"$target_dir"* ]]; then
		secho trouble "Warning: Target directory $target_dir isn't in \$PATH; add it in /etc/profile"
	fi
else
	target_dir="/usr/local/bin"
fi

if [[ ! -d $target_dir ]]; then
	echo && mkdirp "$target_dir" && echo
	created_target_dir=true
fi

warn_if_dir_not_in_path "$target_dir" "$created_target_dir"

# We might need to use sudo
if [[ ! -w $target_dir ]] && command -v sudo > /dev/null; then
	sudo="sudo"
else
	sudo=""
fi

# Create symlinks
for file_rel_path in std/*; do
	install_file "$script_dir/$file_rel_path" "$target_dir/$(basename "$file_rel_path")" "$sudo"
done

if [[ $OSTYPE == "darwin"* ]]; then
	for file_rel_path in mac/*; do
		install_file "$script_dir/$file_rel_path" "$target_dir/$(basename "$file_rel_path")" "ask $sudo"
	done
fi
