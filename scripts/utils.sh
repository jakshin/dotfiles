# Utilities for use in this repo's bash & zsh scripts.

# Ensure /opt/homebrew/bin is in $PATH in all install scripts on M1 macOS
if [[ $OSTYPE == "darwin"* && $HOSTTYPE == "arm64" ]]; then
	[[ -n $HOMEBREW_PREFIX ]] || HOMEBREW_PREFIX="/opt/homebrew"

	if [[ $PATH != *"$HOMEBREW_PREFIX/bin:"* ]]; then
		if [[ $PATH == */usr/local/bin:* ]]; then
			# Add Homebrew's bin folder just after /usr/local/bin
			PATH="${PATH/\/usr\/local\/bin:/\/usr\/local\/bin:$HOMEBREW_PREFIX\/bin:}"
		else
			# Add Homebrew's bin folder at the beginning
			PATH="$HOMEBREW_PREFIX/bin:$PATH"
		fi

		hash -r
	fi
fi

# Special echo
function secho() {
	local n_arg=() color="" no_color=""

	while true; do
		if [[ $1 == "-n" && ${#n_arg[@]} == 0 ]]; then
			n_arg=("-n")
			shift

		elif [[ -z $color ]]; then
			case $1 in
				bright)  color="\033[1m" ;;
				dim)     color="\033[38;5;238m" ;;
				heading) color="\033[38;5;37m"  ;;
				title)   color="\033[38;5;44m"  ;;
				trouble) color="\033[38;5;217m" ;;
			esac

			# shellcheck disable=SC2015
			[[ -n $color ]] && shift || break
		else
			break
		fi
	done

	[[ -z $color ]] || no_color="\033[0m"
	echo -e "${n_arg[@]}" "${color}${*}${no_color}"
}

# Confirms a pending action by prompting the user for a yes/no answer;
# this always reads from the terminal, and doesn't use redirected input
function confirm() {
	local question="$1"
	unset REPLY  # Return value

	while [[ -z $REPLY || ! "YyNn" =~ $REPLY ]]; do
		secho -n bright "${question} [y|n] "

		if [[ -n $ZSH_VERSION ]]; then
			read -rk 1 # Always reads from the terminal, use -u 0 if that's not desired
		else
			read -rn 1 < /dev/tty
		fi

		echo
	done

	[[ $REPLY != "Y" ]] || REPLY="y"
	[[ $REPLY != "N" ]] || REPLY="n"
}

# Lists files in color
function colored_ls() {
	if [[ -z $_colored_ls_opts ]]; then
		# shellcheck disable=SC2010
		if ls --version 2>&1 | grep -Eq "GNU|BusyBox"; then
			_colored_ls_opts="--color=auto -F"
		else
			_colored_ls_opts="-GF"
		fi
	fi

	ls $_colored_ls_opts "$@"
}

# Gets the path of the closest existing ancestor of the given path.
# Paths are never their own ancestors, even if they exist.
# The ancestor path only ends with a slash if it's the root directory.
# If you pass a relative path, you'll get a relative path back.
# WARNING: The returned path exists, but might not be a directory!
#
function get_nearest_existing_ancestor_path() {
	local child_path=$1

	local _nea_path
	_nea_path="$(dirname -- "$child_path")"

	while [[ ! -e $_nea_path ]]; do
		_nea_path="$(dirname -- "$_nea_path")"
	done

	printf "%s\n" "$_nea_path"
}

# Creates the directory recursively, using "mkdir -p", and using sudo if needed,
# based on write permissions in the nearest existing ancestor path
function mkdirp() {
	local dir=$1

	if [[ -d $dir ]]; then
		return 0
	fi

	local ancestor sudo
	ancestor="$(get_nearest_existing_ancestor_path "$dir")"
	[[ -w $ancestor ]] || sudo="sudo"

	echo "Recursively creating directory $dir, starting at $ancestor..."
	$sudo mkdir -p "$dir"
}

function warn_if_dir_not_in_path() {
	local dir=$1
	local no_blank_line=$2

	if [[ $PATH != *"$dir"* ]]; then
		[[ $no_blank_line == true ]] || echo
		secho trouble "Warning: $dir is not in \$PATH"

		if [[ $OSTYPE == "linux-gnu" && $VENDOR == "Solus" ]]; then
			secho trouble "See /usr/share/defaults/etc/profile.d/10-path.sh\n"
		else
			echo
		fi
	fi
}

# Help, sort of
# Always pass the script's own args, like so: show_help_if_requested "$@"
function show_help_if_requested() {
	if [[ $1 == "-h" || $1 == "--help" ]]; then
		local script="$0"  # The script that sourced this file
		if [[ -n $funcfiletrace ]]; then
			script="${funcfiletrace[1]/:*/}"  # On zsh, $0 is the function's name
		fi

		if command -v less > /dev/null; then
			less "$script"
		else
			# Show comments up to the first non-comment or blank line
			sed -E '/^$|^[^#]/q' "$script"
		fi
		exit
	fi
}

# If we're running on MSYS2, we need to be running as Administrator
function ensure_admin_on_msys2() {
	if [[ $OSTYPE == "msys" && -z $_admin_on_msys2 ]]; then
		if ! net session &> /dev/null; then
			secho trouble "You must be running as Administrator to successfully create symlinks on MSYS2"
			exit 1
		fi

		_admin_on_msys2=true
	fi
}

# Makes a symlink, even on MSYS2 and OpenIndiana
function symlink() {
	local linked_path=$1  # Should definitely be absolute
	local link_path=$2    # Can be absolute, or relative to $PWD

	if [[ $OSTYPE == "msys" ]]; then
		# On MSYS2, `ln -s` actually copies files, and we need to use cmd's mklink instead,
		# running as administrator (see https://github.com/msys2/MSYS2-packages/issues/249)
		ensure_admin_on_msys2
		cmd //c mklink "$(cygpath -w "$link_path")" "$(cygpath -w "$linked_path")"
		return $?
	fi

	if [[ ! -w "$(dirname -- "$link_path")" ]] && command -v sudo > /dev/null; then
		local sudo="sudo"  # Use sudo automatically if needed (and it's available)
	else
		local sudo=""
	fi

	if [[ $OSTYPE == "solaris2"* ]]; then
		# On OpenIndiana, ln and mv don't have a "-v" option
		$sudo ln -s "$linked_path" "$link_path"
	else
		$sudo ln -sv "$linked_path" "$link_path"
	fi
}

# Installs a file after prompting for confirmation, handling cases where it already exists
function install_file() {
	local source_path="$1"
	local target_path="$2"

	# Optional; can contain ask, short, and/or sudo
	# ask = ask before installing even if the target symlink doesn't already exist
	# short = display only the target symlink's filename, not its full path
	local flags="$3"

	if [[ $flags == *sudo* ]]; then
		local sudo="sudo"
	else
		local sudo=""
	fi

	local source_file
	source_file="$(basename "$1")"

	local target_display="$target_path"
	if [[ $flags == *short* ]]; then
		target_display="$(basename "$target_path")"
	fi

	local question="Install ${source_file}?"
	local keep_original=false
	local target_exists=false

	if [[ -L "$target_path" ]]; then
		local linked_to
		linked_to="$(readlink "$target_path")"

		if [[ $linked_to == "$source_path" ]]; then
			if [[ $_install_file_confirmed_last_time == true ]]; then
				echo
			fi

			_install_file_ran_already=true
			_install_file_confirmed_last_time=false

			echo "Already exists: ${target_display} -> ${source_path}"
			return
		else
			question="Symlink $target_display exists ($linked_to);\nReplace it?"
			target_exists=true
		fi

	elif [[ -f "$target_path" ]]; then
		if [[ ($source_file == "bash_profile" || $source_file == "bashrc" || $source_file == "zshrc") && \
			$OSTYPE != "darwin"* && ! -f "${target_path}.original" ]];
		then
			local target_basename
			target_basename="$(basename "$target_path")"
			question="File ${target_display} exists; keep it as ${target_basename}.original and then replace it?"
			keep_original=true
		else
			question="File ${target_display} exists; Replace it?"
			target_exists=true
		fi

	elif [[ -d "$target_path" ]]; then
		question="Directory ${target_display} exists; Replace it?"
		target_exists=true

	elif [[ -e "$target_path" ]]; then
		if [[ $_install_file_ran_already == true ]]; then
			echo  # Whether the last file was already installed or not
		fi

		secho trouble "Error: ${target_display} exists and isn't a symlink, normal file or directory; aborting"
		exit 1
	fi

	if [[ $flags == *ask* || $target_exists == true ]]; then
		if [[ $_install_file_ran_already == true ]]; then
			echo  # Whether the last file was already installed or not
		fi

		_install_file_ran_already=true
		_install_file_confirmed_last_time=true

		confirm "$question"
		if [[ $REPLY == "n" ]]; then
			echo "Skipping $source_file"
			return
		fi
	else
		if [[ $_install_file_confirmed_last_time == true ]]; then
			echo
		fi

		_install_file_ran_already=true
		_install_file_confirmed_last_time=false
	fi

	if [[ $keep_original == true ]]; then
		if [[ $OSTYPE == "solaris2"* ]]; then
			# On OpenIndiana, ln and mv don't have a "-v" option
			$sudo mv "$target_path" "${target_path}.original"
		else
			$sudo mv -v "$target_path" "${target_path}.original"
		fi
	fi

	if [[ $target_exists == true ]]; then
		$sudo rm -rf "$target_path"
	fi

	if [[ $flags == *short* ]]; then
		(
			cd "$(dirname "$target_path")" || return 1
			symlink "$source_path" "$target_display"
		)
	else
		symlink "$source_path" "$target_path"
	fi
}
