#!/usr/bin/env zsh
# Based on http://tldp.org/HOWTO/Bash-Prompt-HOWTO/x329.html

if [[ $1 == "-h" || $1 == "--help" ]]; then
	script_name="$(basename -- "$0")"
	echo "Displays the 16 standard terminal colors on its 8 standard background colors, in a test pattern"
	echo "Usage: $script_name"
	exit
fi

bgs=(40m 41m 42m 43m 44m 45m 46m 47m)
fgs=(0m 1m 30m '1;30m' 31m '1;31m' 32m '1;32m' 33m '1;33m' 34m '1;34m' 35m '1;35m' 36m '1;36m' 37m '1;37m')

function color_row() {
	local bg="$1"
	local fg="$2"
	local text="$3"

	local fgstr="${(l:5:: :)fg}"
	local fgesc=""
	[[ -z $fg ]] || fgesc="\033[$fg"

	echo -en " $fgstr $fgesc  $text  "
	for bg in $bgs; do
		echo -en " $fgesc\033[$bg  $text  \033[0m"
	done
	echo
}

echo -en "\n             "
for bg in $bgs; do
	echo -n "       $bg"
done
echo

color_row $bg "" "     "

for fg in $fgs; do
	color_row $bg $fg "\u2588 \u2588 \u2588"
	color_row $bg "" "     "
done
