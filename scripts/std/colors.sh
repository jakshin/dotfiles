#!/usr/bin/env bash

if [[ $1 == "-h" || $1 == "--help" ]]; then
	script_name="$(basename -- "$0")"
	echo "Displays the 16 standard terminal colors (which varies by Terminal theme)"
	echo "Usage: $script_name [-b|--blocks]"
	exit
elif [[ $1 = "-b" || $1 == "--blocks" ]]; then
	block='██'
else
	block=''
fi

echo
echo -e "\033[0m $block \\\033[0m  NO COLOR"

echo
echo -e "\033[0;30m $block \\\033[0;30m  BLACK      (black)"
echo -e "\033[1;30m $block \\\033[1;30m  DARK GRAY  (bright black)"
echo -e "\033[0;37m $block \\\033[0;37m  LIGHT GRAY (white)"
echo -e "\033[1;37m $block \\\033[1;37m  WHITE      (bright white)"

echo
echo -e "\033[0;31m $block \\\033[0;31m  RED       \033[1;31m $block \\\033[1;31m  BRIGHT RED"
echo -e "\033[0;32m $block \\\033[0;32m  GREEN     \033[1;32m $block \\\033[1;32m  BRIGHT GREEN"
echo -e "\033[0;33m $block \\\033[0;33m  YELLOW    \033[1;33m $block \\\033[1;33m  BRIGHT YELLOW"
echo -e "\033[0;34m $block \\\033[0;34m  BLUE      \033[1;34m $block \\\033[1;34m  BRIGHT BLUE"
echo -e "\033[0;35m $block \\\033[0;35m  MAGENTA   \033[1;35m $block \\\033[1;35m  BRIGHT MAGENTA"
echo -e "\033[0;36m $block \\\033[0;36m  CYAN      \033[1;36m $block \\\033[1;36m  BRIGHT CYAN \033[0m"
