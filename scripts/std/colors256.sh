#!/usr/bin/env bash
# See https://www.calmar.ws/vim/256-xterm-24bit-rgb-color-chart.html for hex codes

if [[ $1 == "-h" || $1 == "--help" ]]; then
	script_name="$(basename -- "$0")"
	echo "Displays the 256 standard terminal colors (which shouldn't vary)"
	echo "Usage: $script_name [-b|--blocks]"
	exit
elif [[ $1 = "-b" || $1 == "--blocks" ]]; then
	block='██ '
	esc=''
	pad_width=13
else
	block=''
	esc='\\033'
	pad_width=17
fi

function echo_color() {
	local color_num="$1"
	local escape="\033[38;5;${color_num}m"
	local quoted="${esc}[38;5;${color_num}m"
	printf -v padded "%-${pad_width}s" "$quoted"
	echo -en "${escape}${block}${padded}"
}

# Regular and "bold" ansi colors
for n in {0..7};  do echo_color $n; done; echo
for n in {8..15}; do echo_color $n; done; echo

# Extended colors, in a 6x6x6 grid
for n in {16..231}; do
	[[ $(((n - 16) % 6)) == 0 ]] && echo
	echo_color $n;
done
echo

# Grayscale
vertical=false

for n in {232..255}; do
	if [[ $vertical == true || $(((n - 232) % 8)) == 0 ]]; then
		echo
	fi

	echo_color $n;
done

# Reset
echo -e "\033[0m"
