#!/usr/bin/env bash
set -e
script_name="${0##*/}"

function usage() {
	echo "Removes every node_modules directory under a target directory"
	echo "(or the current directory by default)."
	echo
	echo "Usage: $script_name [directory] [options]"
	echo
	echo "Options may be given before or after the directory name:"
	echo "-f, --force  Delete node_modules directories without confirmation"
	echo "             (Otherwise they're listed and a prompt is displayed)"
	exit "$1"
}

script_dir="$(dirname "$(readlink "${BASH_SOURCE[0]}" || echo "${BASH_SOURCE[0]}")")"
source "$script_dir/../../scripts/utils.sh"

# Parse the command line
unset root_dir
force=false

for arg; do
	if [[ $arg == '-h' || $arg == '--help' ]]; then
		usage 0
	elif [[ $arg == '-f' || $arg == '--force' ]]; then
		force=true
	elif [[ $arg == -* ]]; then
		secho trouble "Error: Invalid option: $arg\n"
		usage 1
	elif [[ -z $root_dir ]]; then
		root_dir="$arg"
	else
		secho trouble "Error: Unexpected argument: $arg"
		echo -e "You may specify only one target directory\n"
		usage 1
	fi
done

[[ -n $root_dir ]] || root_dir="."
if [[ $root_dir == "." ]]; then
	root_dir_desc="the current directory"
else
	root_dir_desc="$root_dir"
fi

# Check whether the directory exists
if [[ ! -d $root_dir ]]; then
	secho trouble "Error: Directory \"$root_dir\" doesn't exist\n"
	usage 1
fi

# Find top-level node_modules directories
secho heading "Looking for node_modules directories..."
nm_dirs="$(find "$root_dir" -name "node_modules" -type d -not -ipath "*/node_modules/*")"

if [[ -z $nm_dirs ]]; then
	echo "👍 No node_modules directories found under $root_dir_desc"
	exit 0
fi

nm_dir_count="$(echo "$nm_dirs" | wc -l | xargs)"

if [[ $nm_dir_count == 1 ]]; then
	directories="directory"
	them="it"
else
	directories="directories"
	them="them"
fi

echo "📂 Found $nm_dir_count node_modules $directories under $root_dir_desc"

# Confirm, if needed
if [[ $force == false ]]; then
	echo -e "\n$nm_dirs"
	confirm "Remove $them?"

	if [[ $REPLY == "n" ]]; then
		echo "Aight"
		exit 0
	fi
fi

# Remove the node_modules directories
# See https://stackoverflow.com/a/11003457 for details on using `xargs -I` safely
echo
secho heading "Removing node_modules $directories..."

export green_checkmark="\033[38;5;34m✔\033[0m"
echo "$nm_dirs" | tr '\n' '\0' |
	xargs -0 -n 1 -P 6 -I {} bash -c 'rm -rf "$@" && echo -e "${green_checkmark} $@"' _ {}
