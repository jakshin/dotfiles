#!/usr/bin/env bash
set -e
script_name="${0##*/}"

function usage() {
	echo "Zips up any number of VMware virtual machines to like-named zip files,"
	echo "in a Backups subdirectory, which will be created if it doesn't yet exist."
	echo
	echo "Usage: $script_name VM.vmwarevm [...]"
	exit "$1"
}

script_dir="$(dirname "$(readlink "${BASH_SOURCE[0]}" || echo "${BASH_SOURCE[0]}")")"
source "$script_dir/../../scripts/utils.sh"

declare -a vms=()

for arg; do
	if [[ $arg == "-h" || $arg == "--help" ]]; then
		usage 0
	elif [[ $arg == -* ]]; then
		secho trouble "Error: Invalid option: $arg\n"
		usage 1
	fi
	
	vm="${arg%/}"
	vms+=("$vm")
	
	if [[ ! -d $vm || $vm != *.vmwarevm ]]; then
		secho trouble "Error: \"$vm\" does not appear to be a VMware VM\n"
		usage 1
	fi
done

if [[ ${#vms[@]} == 0 ]]; then
	usage 1
fi

# Zip up up the VM(s)
all_started="$( date +%s )"
last_was_error=false
errors=0
zipped=0
first=true

for vm in "${vms[@]}"; do
	vm_dir="$( dirname "$vm" )"
	vm_file="$( basename "$vm" )"
	vm_basename="${vm_file%.*}"
	backup_dir="$vm_dir/Backups"
	zip_path="$backup_dir/$vm_basename ($( date -j +%Y-%m-%d )).zip"
	
	if [[ -e $zip_path ]]; then
		[[ $first == true || $last_was_error == true ]] || echo
		secho trouble "Error: zip file already exists: $zip_path"
		first=false
		last_was_error=true
		(( errors++ ))
		continue
	else
		last_was_error=false
	fi

	[[ $first == true ]] && first=false || echo
	secho heading "Zipping $vm_file => $zip_path"
	started="$( date +%s )"
	
	if [[ ! -d $backup_dir ]]; then
		mkdir -p "$backup_dir"
	fi
	
	zip -r "$zip_path" "$vm"
	(( zipped++ ))

	finished="$( date +%s )"
	runtime_seconds="$(( finished - started ))"
	runtime="$( date -u -r $runtime_seconds +%T )"
	secho dim "Zipped $vm_file in $runtime"
done

# Report what happend
if (( zipped > 0 )); then
	all_finished="$( date +%s )"
	runtime_seconds="$(( all_finished - all_started ))"
	runtime="$( date -u -r $runtime_seconds +%T )"

	if [[ $zipped == 1 ]]; then
		vm_count_str="1 VM"
	else
		vm_count_str="$zipped VMs"
	fi

	if [[ $errors == 0 ]]; then
		secho dim "Zipped $vm_count_str in $runtime"
	elif [[ $errors == 1 ]]; then
		secho trouble "Zipped $vm_count_str in $runtime, but 1 error occurred"
	else
		secho trouble "Zipped $vm_count_str in $runtime, but $errors errors occurred"
	fi
else
	if [[ $errors == 1 ]]; then
		secho trouble "Bummer: 1 error occurred"
	else
		secho trouble "Bummer: $errors errors occurred"
	fi
fi

exit "$errors"
