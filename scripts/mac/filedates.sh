#!/bin/sh

echo "[Last Modified]        [Created]              File Name"
echo "---------------------- ---------------------- --------------------------------------------------"

if [ $# -eq 0 ]; then
	stat -f "[%Sm] [%SB] %N" -t "%D %r" -- *
else
	stat -f "[%Sm] [%SB] %N" -t "%D %r" -- "$@"
fi
