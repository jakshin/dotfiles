#!/usr/bin/env bash
# Runs gradlew, if it's found in the current directory or parents (up to $HOME or the root),
# falling back to system-wide gradle otherwise.

# Figure out what to do
set -eu
gradle_cmd=""
dir="${PWD%/}"  # Might also consider using `pwd -P`

while true; do
	if [[ -x "$dir/gradlew" ]]; then
		gradle_cmd="$dir/gradlew"
		builtin cd "$dir"  # Need to run gradlew from its own directory
		break
	fi
	
	if [[ $dir == "" || $dir == "$HOME" ]]; then
		break  # Can't find a gradlew
	fi

	dir="$(dirname "$dir")"
	[[ $dir == "/" ]] && dir=""
done

if [[ -z $gradle_cmd ]]; then
	gradle_cmd="$(command which gradle || echo -n)"

	if [[ -z $gradle_cmd ]]; then
		echo "Error: Couldn't find a gradlew or gradle to run"
		exit 1
	fi
fi

# Show what we'll do
args=""
whitespace="[[:space:]]+"

for arg; do
	[[ -n $args ]] && args+=' '

	if [[ $arg =~ $whitespace ]]; then
		if [[ $arg =~ \' && ! $arg =~ \$ ]]; then
			args+="\"$arg\""
		else
			args+="'$arg'"  # Any single quotes in $arg aren't escaped, oh well
		fi
	else
		args+="$arg"
	fi
done

dim='\033[38;5;238m'
normal='\033[0m'
echo -e "${dim}Running $gradle_cmd ${args}${normal}"

# Do it
"$gradle_cmd" "$@"
