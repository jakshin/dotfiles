# ~/.bash_profile: Executed by bash for login shells, after /etc/profile.
# On Cygwin, look in /etc/defaults/etc/skel/ for the default version of this file.

# Homebrew installs to different places on x86 and M1
if [[ $OSTYPE == "darwin"* ]]; then
	if [[ -z $HOMEBREW_PREFIX || -z $HOMEBREW_REPOSITORY ]]; then
		# Run `brew shellenv` for these values and more
		if [[ $CPUTYPE == "x86_64" ]]; then
			export HOMEBREW_PREFIX="/usr/local"
			export HOMEBREW_REPOSITORY="/usr/local/Homebrew"
		elif [[ $CPUTYPE == "arm64" ]]; then
			export HOMEBREW_PREFIX="/opt/homebrew"
			export HOMEBREW_REPOSITORY="/opt/homebrew"
		fi
	fi

	if [[ $PATH != *"$HOMEBREW_PREFIX/bin:"* ]]; then
		if [[ $PATH == */usr/local/bin:* ]]; then
			# Add Homebrew's bin folder just after /usr/local/bin
			PATH="${PATH/\/usr\/local\/bin:/\/usr\/local\/bin:$HOMEBREW_PREFIX\/bin:}"
		else
			# Add Homebrew's bin folder at the beginning
			PATH="$HOMEBREW_PREFIX/bin:$PATH"
		fi
	fi
fi

# If this login shell is interactive, then source .bashrc if it exists
# (Don't source .bashrc for non-interactive shells, to avoid getting a handful
# of "bind: warning: line editing not enabled" errors printed to the console,
# e.g. when right-clicking on a folder and using "Bash Prompt Here" in Windows)
if [[ $- =~ i ]]; then
	if [[ -f "${HOME}/.bashrc" ]]; then
		source "${HOME}/.bashrc"
	fi
fi

# Initialize pyenv, if it's installed (we do this in bashrc too)
if [[ -x ~/.pyenv/bin/pyenv ]] && ! command -v pyenv > /dev/null; then
	# pyenv was installed by cloning its repo to ~/.pyenv, not via package manager
	PATH="$HOME/.pyenv/bin:$PATH"
fi

if [[ "$(command -v pyenv)" == /* ]]; then
	# pyenv is installed, but hasn't been initialized in this shell instance
	export PYENV_ROOT="$HOME/.pyenv"
	eval "$(pyenv init -)"
fi
