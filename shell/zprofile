if [[ $OSTYPE == "darwin"* ]]; then
	# Disable Big Sur and later's session save/restore mechanism for zsh
	# (Bash's sessions are still disabled by ~/.bash_sessions_disable)
	export SHELL_SESSIONS_DISABLE=1

	# Homebrew installs to different places on x86 and M1
	if [[ -z $HOMEBREW_PREFIX || -z $HOMEBREW_REPOSITORY ]]; then
		# Run `brew shellenv` for these values and more
		if [[ $CPUTYPE == "x86_64" ]]; then
			export HOMEBREW_PREFIX="/usr/local"
			export HOMEBREW_REPOSITORY="/usr/local/Homebrew"
		elif [[ $CPUTYPE == "arm64" ]]; then
			export HOMEBREW_PREFIX="/opt/homebrew"
			export HOMEBREW_REPOSITORY="/opt/homebrew"
		fi
	fi

	if [[ $path[(Ie)$HOMEBREW_PREFIX/bin] == 0 ]]; then
		if [[ $path[(Ie)/usr/local/bin] != 0 ]]; then
			# Add Homebrew's bin folder just after /usr/local/bin
			PATH="${PATH/\/usr\/local\/bin://usr/local/bin:$HOMEBREW_PREFIX/bin:}"
		else
			# Add Homebrew's bin folder at the beginning
			PATH="$HOMEBREW_PREFIX/bin:$PATH"
		fi
	fi
fi

# Initialize pyenv, if it's installed (we do this in bashrc too)
if [[ -x ~/.pyenv/bin/pyenv ]] && ! command -v pyenv > /dev/null; then
	# pyenv was installed by cloning its repo to ~/.pyenv, not via package manager
	PATH="$HOME/.pyenv/bin:$PATH"
fi

if [[ "$(command -v pyenv)" == /* ]]; then
	# pyenv is installed, but hasn't been initialized in this shell instance
	export PYENV_ROOT="$HOME/.pyenv"
	eval "$(pyenv init -)"
fi
