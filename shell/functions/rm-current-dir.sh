# Deletes the current directory, if it's empty or only contains ".DS_Store".
# This works in both bash and zsh; it's referenced from ~/.bashrc and ~/.zshrc.
#
function rm-current-dir() {
	if [[ $# != 0 ]]; then
		echo "Usage: rm-current-dir"
		return
	fi

	if [[ -o pipefail ]]; then
		local pipefail=true
	else
		local pipefail=false
		set -o pipefail
	fi

	local file_count
	file_count=$(command ls -A1 | wc -l | xargs echo)
	local exit_code=$?

	if [[ $exit_code == 0 ]]; then
		if [[ $file_count == 0 || ($file_count == 1 && -f ".DS_Store") ]]; then
			local dir="$PWD"
			rm -f ".DS_Store"
			cd .. && rmdir "$dir"
			exit_code=$?
		else
			echo "Error: the current directory isn't empty"
			exit_code=1
		fi
	fi

	[[ $pipefail == true ]] || set +o pipefail
	return $exit_code
}
