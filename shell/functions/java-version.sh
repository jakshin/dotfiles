# Switches macOS's java version, in this shell instance.
# If it needs to be set for all shell instances, hard-code $JAVA_HOME in ~/.zshrc.
#
# If it needs to be overridden for GUI programs, try something like this:
# launchctl setenv JAVA_HOME "$(/usr/libexec/java_home -v 1.8)"
#
function java-version() {
	local arg="$1"  # An option (-h, --help, -l, --list), or a version like 1.8, 11, or 17

	if [[ $arg == "-h" || $arg == "--help" ]]; then
		echo "Usage: java-version            # Show the version currently in use"
		echo "       java-version version    # Use the given version, e.g. 1.8 or 17"
		echo "       java-version -l|--list  # List all installed versions"

	elif ! /usr/libexec/java_home &> /dev/null; then
		# There are probably no Java runtimes installed
		/usr/libexec/java_home
		return 1

	elif [[ $arg == "-l" || $arg == "--list" ]]; then
		# Strip the last line of java_home's output, which shows the default version
		# (NOT the version currently in use via $JAVA_HOME, which I find confusing)
		/usr/libexec/java_home -V | sed -e '$ d'

	elif [[ -n $arg ]]; then
		# Attempting to set the current version
		if /usr/libexec/java_home -F -v "$arg" &> /dev/null; then
			JAVA_HOME="$(/usr/libexec/java_home -F -v "$arg")"
			export JAVA_HOME
			echo "$JAVA_HOME"
		else
			echo "Error: Java version $arg isn't installed."
			echo -e "Installed versions are listed below.\n"
			/usr/libexec/java_home -V | sed -e '$ d'
			return 1
		fi

	else
		# Show the version in use now (if $JAVA_HOME is set, we assume it's valid)
		if [[ -n $JAVA_HOME ]]; then
			echo "$JAVA_HOME"
		else
			/usr/libexec/java_home
		fi
	fi
}

if [[ $USER == "jason.jackson" ]] && ! /usr/libexec/java_home &> /dev/null; then
	# Due I guess to how Self Service installs Zulu JDKs,
	# /usr/libexec/java_home can't find or use them, even if $JAVA_HOME is set,
	# and always hangs or just shows an "Unable to locate a Java Runtime" error
	# (All still works as expected with a JDK installed via Homebrew)

	function java-version() {
		local arg="$1"  # An option (-h, --help, -l, --list), or a version/subdir like zulujdk_21

		local jvms_dir default_jvm
		jvms_dir="/Library/Java/JavaVirtualMachines"
		default_jvm="$(command ls -1r "$jvms_dir" | head -n1)"  # Latest version, could be empty

		if [[ $arg == "-h" || $arg == "--help" ]]; then
			echo "Usage: java-version            # Show the version currently in use"
			echo "       java-version version    # Use the given version, e.g. ${default_jvm:-zulujdk_21}"
			echo "       java-version -l|--list  # List all installed versions"

		elif [[ $arg == "-l" || $arg == "--list" ]]; then
			local found_jvm=false
			for item in $(command ls -1r "$jvms_dir" 2> /dev/null); do
				if [[ -d "$jvms_dir/$item" ]]; then
					if [[ $found_jvm == false ]]; then
						echo "Installed Java versions:"
						found_jvm=true
					fi

					echo "* $item"
				fi
			done

			if [[ $found_jvm == false ]]; then
				echo "No Java versions are installed in $jvms_dir"
			fi
		elif [[ -n $arg ]]; then
			# Attempting to set the current version
			if [[ -d "$jvms_dir/$arg" ]]; then
				local old_java_home=$JAVA_HOME
				export JAVA_HOME="$jvms_dir/$arg"
				if java -version; then
					echo && echo "\$JAVA_HOME: $JAVA_HOME"
				else
					JAVA_HOME=$old_java_home
					return 1
				fi
			else
				echo "Error: Java version \"$arg\" isn't installed"
				java-version --list  # Recursive call
				return 1
			fi
		else
			# Show the version in use now
			local java_works
			if java -version; then
				java_works=true
				echo  # The system `java` wrapper outputs its own blank line if it can't find a JVM
			else
				java_works=false
			fi

			if [[ -z $JAVA_HOME ]]; then
				echo -n "\$JAVA_HOME isn't set"
				if [[ $java_works == false ]]; then
					echo "! That's a problem, you should set it"
				else
					echo # End the "isn't set" line
				fi
			else
				echo "\$JAVA_HOME: $JAVA_HOME"
			fi

			[[ $java_works == true ]]
		fi
	}
fi
