Lazily loaded functions for bash and zsh.   
They're referenced in `~/.bashrc` and `~/.zshrc`.   
Also see the [zsh/functions](../../zsh/functions/) directory.
