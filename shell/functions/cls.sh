# Clears the screen and the scrollback buffer.
#
# This is useful on macOS (Apple Terminal, iTerm, Tabby), XTerm on BSDs,
# sometimes MATE Terminal and Xfce Terminal (but sometimes not),
# and MobaXterm local terminals on Windows 10/11.
#
# It would also be useful on Tabby on Windows 10 (Cygwin/MSYS2/WSL)
# and Haiku R1 beta3's terminal, if it worked there... but it doesn't.
#
# It's unneeded but harmless everywhere else I've tried, i.e. plain "clear"
# clears the scrollback buffer too: Linux distros' terminals and XTerms,
# Cygwin/MSYS2/WSL in any terminal emulator, including MobaXterm WSL,
# and Git for Windows bash.
#
function cls() {
	clear
	printf '\e[3J'
}
