# Sends the current directory to the Trash, regardless of its contents.
# The 'trash' command must exist (can be an alias or function).
# This works in both bash and zsh; it's referenced from ~/.bashrc and ~/.zshrc.
#
function trash-current-dir() {
	if [[ $# != 0 ]]; then
		echo "Usage: trash-current-dir"
		return
	elif ! builtin type "trash" &> /dev/null; then
		echo "Error: 'trash' command not found"
		return 1
	fi

	local dir="$PWD"
	cd .. && trash "$dir"
}
