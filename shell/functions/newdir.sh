# Creates a new directory at the given path, then makes it the working directory.
# Accepts -m MODE, -p and -v options, which will be passed to mkdir.
# This works in both bash and zsh; it's referenced from ~/.bashrc and ~/.zshrc.
#
function newdir() {
	if [[ -n $ZSH_VERSION ]]; then
		emulate -L zsh
		setopt sh_word_split
	fi

	local arg i
	local expecting_mode=false opts_done=false help=false
	local mkdir_opts=() new_path=""

	for arg; do
		if [[ $expecting_mode == true ]]; then
			mkdir_opts+=(-m "$arg")
			expecting_mode=false
		elif [[ $opts_done == true || $arg == - || $arg != -* ]]; then
			if [[ -n $new_path ]]; then
				# Error: only one new path is accepted
				unset new_path
				break
			else
				new_path=$arg
				opts_done=true
			fi
		elif [[ $arg == "--" ]]; then
			opts_done=true
		elif [[ $arg == "-h" || $arg == "--help" ]]; then
			help=true
			break
		elif [[ $arg != --* && $arg != *" "* ]]; then
			for (( i=1; i < ${#arg}; i++ )); do
				local ch="${arg:$i:1}"

				if [[ $ch == "p" || $ch == "v" ]]; then
					mkdir_opts+=("-$ch")
				elif [[ $ch == "m" ]]; then
					# The rest of $arg is the mode, or the next arg is
					(( i++ ))
					local mode=${arg:$i}
					if [[ -n $mode ]]; then
						mkdir_opts+=(-m "$mode")
					else
						expecting_mode=true
					fi
					break
				else
					echo -e "Error: invalid option: $ch\n"
					break 2
				fi
			done
		else
			# Begins with dash(es), but isn't a valid option
			echo -e "Error: invalid option: $arg\n"
			break
		fi
	done

	if [[ $help == true || -z $new_path ]]; then
		echo "Creates a new directory at the given path, then makes it the working directory."
		echo "Accepts -m MODE, -p and -v options, which will be passed to mkdir."
		echo
		echo "Usage: newdir [options] path-to-directory"

		if [[ $help == true || $# == 0 ]]; then
			return 0
		else
			return 1
		fi
	fi

	if [[ $new_path == - ]]; then
		new_path=./-
	fi

	mkdir "${mkdir_opts[@]}" -- "$new_path" || return 1
	cd -- "$new_path" || return 1
}

if false; then
	function echo_test_error() {
		printf "\033[1;31m%s\033[0m\n" "$*"
	}

	function test_newdir() {
		if [[ -n $test_divider ]]; then
			echo "$test_divider"
		fi

		local expected=$1
		shift

		local expected_dir arg
		for arg; do
			expected_dir=$arg
		done

		if [[ $expected_dir == */* ]]; then
			expected_dir="$(basename -- "$expected_dir")"
		fi

		(
			echo -n "Expecting $expected with arguments:"
			for arg; do
				if [[ -z $arg ]]; then
					echo -n ' ""'
				elif [[ $arg == *" "* ]]; then
					echo -n " \"$arg\""
				else
					printf " %s" "$arg"
				fi
			done
			echo

			newdir "$@"
			local result=$?

			if [[ $expected == success ]]; then
				local current_dir
				current_dir="$(basename "$PWD")"
				[[ $current_dir == "$expected_dir" ]] || echo_test_error "PWD isn't $expected_dir"
				[[ $result == 0 ]] || echo_test_error "newdir returned $result"
				printf "==> PWD: %s\n" "$PWD"
				ls -ld "$PWD"
			elif [[ $expected == error ]]; then
				[[ $PWD == ~/Scratch ]] || echo_test_error "PWD is $PWD"
				[[ $result != 0 ]] || echo_test_error "newdir returned $result"
			elif [[ $expected == usage ]]; then
				[[ $PWD == ~/Scratch ]] || echo_test_error "PWD is $PWD"
				[[ $result == 0 ]] || echo_test_error "newdir returned $result"
			else
				echo_test_error "WTF is $expected?"
			fi
		)
	}

	cd ~/Scratch || exit 1
	test_divider=$(printf '%.0s-' {1..90})
	rm -rf "new dir"* - -- --help -h -pv "Jason's"*

	# Success expected
	test_newdir success -m777 -pv "new dir 1/subdir"
	test_newdir success -pppvvv -m 000 -m 777 "new dir 2"
	test_newdir success -m g-rwx -p "new dir 3"
	test_newdir success -mg-rwx "new dir 4"
	test_newdir success -
	test_newdir success -p -
	test_newdir success -- --
	test_newdir success -- --help
	test_newdir success -- -h
	rest_newdir success -- -pv
	test_newdir success "Jason's \"favorite\""$'\t'"\test"

	# Error expected
	test_newdir error -x xdir1
	test_newdir error -px xdir2
	test_newdir error g-rwx xdir3
	test_newdir error xdir4 xdir5
	test_newdir error "-p -v" xdir6
	test_newdir error -pvh xdir7  # Only -h alone requests help
	test_newdir error -v ""
	test_newdir error ""
	test_newdir error --
	test_newdir error "new dir 1"  # We created the dir above, and -p wasn't passed

	# Usage expected
	test_newdir usage
	test_newdir usage --help
	test_newdir usage -h xdir8

	if ls -ld xdir* &> /dev/null; then
		echo "$test_divider"
		echo_test_error "Oops..."
		ls -ld xdir*
		rm -rf xdir*
	fi

	unset -f echo_test_error test_newdir
	unset test_divider
fi
