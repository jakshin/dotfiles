#!/usr/bin/env bash
# Installs symlinks in $HOME for files in this directory.

set -e
script_dir="$(cd -- "$(dirname -- "$0")" && pwd -P)"
source "$script_dir/../scripts/utils.sh"
show_help_if_requested "$@"
cd -- "$script_dir"  # Run from this script's directory
ensure_admin_on_msys2
secho title "Installing shell dotfiles..."

if [[ $OSTYPE != "haiku" ]]; then
	# Haiku's bash doesn't notice dotfiles in $HOME
	install_file "$script_dir/bash_profile" ~/.bash_profile
	install_file "$script_dir/bashrc" ~/.bashrc
fi

install_file "$script_dir/zprofile" ~/.zprofile
install_file "$script_dir/zshrc" ~/.zshrc

if [[ $OSTYPE == "darwin"* || $OSTYPE == "haiku" || $OS == "Windows"* || -n $WSL_DISTRO_NAME ]]; then
	# My yazptrc is currently useful on macOS, Haiku, and Windows/WSL
	install_file "$script_dir/yazptrc" ~/.yazptrc
fi

if [[ $OSTYPE == "darwin"* ]]; then
	echo
	install_file "$script_dir/bash_sessions_disable" ~/.bash_sessions_disable
	install_file "$script_dir/hushlogin" ~/.hushlogin
fi
