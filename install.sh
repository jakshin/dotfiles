#!/usr/bin/env bash
# Installs all the things, by calling other install scripts.
# shellcheck disable=SC2207

set -e
script_dir="$(cd -- "$(dirname -- "$0")" && pwd -P)"
source "$script_dir/scripts/utils.sh"
show_help_if_requested "$@"
cd -- "$script_dir"  # Run from this script's directory
ensure_admin_on_msys2

function get_environment_name() {
	if [[ -n $env_name ]]; then
		return

	elif [[ $OSTYPE == "darwin"* ]]; then
		env_name="macOS"

	elif [[ $OSTYPE == "cygwin" ]]; then
		if [[ $PUTTYHOME == *"MobaXterm"* ]]; then
			env_name="MobaXterm"
		else
			env_name="Cygwin"
		fi

	elif [[ $OSTYPE == "linux-gnu" && -n $WSL_DISTRO_NAME ]]; then
		env_name="$WSL_DISTRO_NAME/WSL"

	elif [[ -e /etc/os-release ]]; then
		eval "$(grep ^NAME= /etc/os-release)"
		env_name=$NAME
	else
		env_name="$(uname -s)"  # e.g. "Haiku", "Linux", "MidnightBSD"
	fi
}

get_environment_name
secho title "Installing on $env_name"

# We expect some programs to already be installed
expected=()

if [[ $env_name != "macOS" ]]; then
	expected+=(curl less nano zsh)

	if [[ $env_name == "Solus" ]] && ! command -v source-highlight > /dev/null; then
		expected+=(highlight)
	elif [[ $env_name != "Haiku" && $env_name != "MidnightBSD" ]]; then
		expected+=(source-highlight)
	fi

	if [[ $env_name != "Haiku" && $OS != "Windows"* ]]; then
		expected+=(micro)
	fi
fi

missing=()
for program in "${expected[@]}"; do
	if ! command -v "$program" > /dev/null; then
		missing+=("$program")
	fi
done

if [[ ${#missing[@]} != 0 ]]; then
	sorted=($(printf '%s\n' "${missing[@]}" | sort))
	echo -e $'\n'"Some programs that are expected to be installed aren't: ${sorted[*]}"
	confirm "Continue with installation anyway?"
	[[ $REPLY == "y" ]] || exit
fi

# Runs an install script, pausing if it exits with an error
function run_install_script() {
	echo
	if ! "./$1"; then
		confirm "Continue overall installation?"
		[[ $REPLY == "y" ]] || exit
	fi
}

function note() {
	echo
	secho title "$@"
}

# Run the various install scripts
run_install_script "git-settings/install.sh"

if [[ $env_name == "macOS" ]]; then
	run_install_script "homebrew/install.sh"
fi

if [[ $env_name == "Haiku" || $env_name == "MidnightBSD" ]]; then
	note "Skipping syntax highlighting script for Less on $env_name"
	echo "(Because source-highlight isn't available for installation here)"
else
	run_install_script "less/install.sh"
fi

if [[ $env_name == "Haiku" || $OS == "Windows"* ]]; then
	note "Skipping settings for the micro editor on $env_name"

	if [[ $env_name == "Haiku" ]]; then
		echo "(Because micro doesn't run on Haiku)"
	else
		# Cygwin & MSYS2 aren't officially supported: https://github.com/zyedidia/micro#cygwin-mingw-plan9
		# On Cygwin it can't open unix-style paths, including through symlinks, so including my settings files
		# On MSYS2 it hangs just after launch in mintty, and doesn't draw anything on screen
		# (although it does work in other terminals)
		echo "(Because Windows micro doesn't work well on $env_name)"
	fi
else
	run_install_script "micro/install.sh"
fi

if [[ $env_name == "MobaXterm" ]]; then
	note "Skipping settings for the nano editor on $env_name"
	echo "(Because the version of nano available here is ancient)"
else
	run_install_script "nano/install.sh"
fi

run_install_script "scripts/install.sh"
run_install_script "shell/install.sh"

if [[ $env_name == "Haiku" ]]; then
	note "Skipping settings for the vim editor on $env_name"
	echo "(Because the Nord colors don't mesh well with Terminal's default color themes)"
else
	run_install_script "vim/install.sh"
fi

if ! command -v zsh > /dev/null; then
	note "Skipping zsh completions and plugins"
	echo "(Because zsh itself isn't yet installed)"
else
	run_install_script "zsh/install.sh"
fi
